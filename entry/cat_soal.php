<div class="progress">
  <div class="progress-bar" id="progress_soal" role="progressbar" aria-valuemax="100">
  </div>
</div>
<?php



require '../system/function.php';
$db = new crud();

//cari soal yang sudah dijawab untuk progress
$sudah        = $db->query("SELECT * FROM `t_cat` WHERE `id_ujian`='" . $_SESSION['id_ujian'] . "' AND `nomor_induk_siswa`='" . substr($_SESSION['username'], 2) . "'")->fetch_all(MYSQLI_ASSOC);
//$sip = $sudah;
if (count($sudah) == 0) {
  $dijawab      = 0;
} else {
  $dijawab      = count($sudah);
}

//mencari mapel yang diujikan
$query = $db->query("SELECT * FROM `t_ujian` WHERE `id_ujian`='" . $_SESSION['id_ujian'] . "'");
$query = $query->fetch_all(MYSQLI_ASSOC);


?>
<script type="text/javascript">
    $(document).ready(function() {
      var dijawab     = <?php echo $dijawab;?>;
      var total_soal  = <?php echo $query[0]['jumlah_soal'];?>;
      var prog = (100/total_soal*dijawab);
      if (prog >= 100) {
        $("#progress_soal").css('width', '100%').html('100%');
      } else {
        $("#progress_soal").css('width', prog + '%').html( Math.floor(prog) + '%');
      }
    });
</script>
<?php
  


  if (!empty($_SESSION['id_ujian'])) {
    //mencari mapel yang diujikan
    $nik          = substr($_SESSION['username'], 2);
    $id_ujian     = $_SESSION['id_ujian'];
    $kelas       = $_SESSION['kelas'];

   
   $soal = $db->query("SELECT * FROM `t_soal` WHERE `id_kelas` = '{$kelas}' AND `kode_mapel` = '{$query[0]['kode_mapel']}' AND `id_soal` NOT IN (SELECT `list_soal` as `soal` FROM `t_cat` WHERE `id_ujian`='{$_SESSION['id_ujian']}' AND `nomor_induk_siswa`='{$nik}') ORDER BY RAND() LIMIT 1")->fetch_all(MYSQLI_ASSOC);

    
   if (count($sudah) == $query[0]['jumlah_soal']) {
    $hasil = 0;
   } else {
    $hasil = count($soal);
   }
    
      
  } else {
    header('location: ' . base_url('/dist/ujian.php'));
  }
    
  if ($hasil > 0) {

      if (isset($_GET['s']) == 'back') {
        // untuk button back
        $_SESSION['page']++;
        $a = count($sudah)-$_SESSION['page'];

        if ($a < 0) {
          $a = 0;
        }

        $soal = $db->query("SELECT `t_cat`.`list_jawaban`,`t_soal`.`id_soal`,`t_soal`.`soal`,`t_soal`.`opsi_a`,`t_soal`.`opsi_b`,`t_soal`.`opsi_c`,`t_soal`.`opsi_d`,`t_soal`.`opsi_e`,`id_kelas` FROM `t_cat`
        LEFT JOIN `t_soal` ON `t_cat`.`list_soal` = `t_soal`.`id_soal`
        WHERE `id_kelas` = '{$kelas}' AND `id_ujian`='{$_SESSION['id_ujian']}' AND `nomor_induk_siswa`='{$nik}' LIMIT {$a}, 1");
        
        foreach ($soal as $value) {
            ?>
            <?php echo $value['soal'];?>
            <form method="post" name="form_soal">
              <input type="hidden" name="soal" value="<?php echo $value['id_soal'];?>">
              <div class="radio">
                <label>
                  <?php
                    if ($value['list_jawaban'] == 'a') {
                      echo '<input type="radio" id="jawab" name="jawaban" value="a" checked>' . $value['opsi_a'];
                    } else {
                      echo '<input type="radio" id="jawab" name="jawaban" value="a">' . $value['opsi_a'];
                    }
                  ?>
                </label>
              </div>

              <div class="radio">
                <label>
                  <?php
                    if ($value['list_jawaban'] == 'b') {
                      echo '<input type="radio" id="jawab" name="jawaban" value="b" checked>' . $value['opsi_b'];
                    } else {
                      echo '<input type="radio" id="jawab" name="jawaban" value="b">' . $value['opsi_b'];
                    }
                  ?>
                </label>
              </div>

              <div class="radio">
                <label>
                  <?php
                    if ($value['list_jawaban'] == 'c') {
                      echo '<input type="radio" id="jawab" name="jawaban" value="c" checked>' . $value['opsi_c'];
                    } else {
                      echo '<input type="radio" id="jawab" name="jawaban" value="c">' . $value['opsi_c'];
                    }
                  ?>
                </label>
              </div>

              <div class="radio">
                <label>
                  <?php
                    if ($value['list_jawaban'] == 'd') {
                      echo '<input type="radio" id="jawab" name="jawaban" value="d" checked>' . $value['opsi_d'];
                    } else {
                      echo '<input type="radio" id="jawab" name="jawaban" value="d">' . $value['opsi_d'];
                    }
                  ?>
                </label>
              </div>

              <div class="radio">
                <label>
                  <?php
                    if ($value['list_jawaban'] == 'e') {
                      echo '<input type="radio" id="jawab" name="jawaban" value="e" checked>' . $value['opsi_e'];
                    } else {
                      echo '<input type="radio" id="jawab" name="jawaban" value="e">' . $value['opsi_e'];
                    }
                  ?>
                </label>
              </div>

              <div class="col-md-2">
                <?php 
                if ($a != 0) {
                  echo '<a href="#" class="btn btn-default" id="back">Back</a>';
                }
              ?>
                <a href="#" class="btn btn-primary" id="next">Next</a>
              </div>

              <div class="col-md-offset-8 col-md-2">
                <strong><?php echo $dijawab;?></strong> / <?php echo $query[0]['jumlah_soal'];?>
              </div>
            </form>
            <?php
        }

      } else {
        
        //membuat validasi next jika tidak back
        if ($_SESSION['page'] <= 1) {
          
          $_SESSION['page'] = 0;

          if ($query[0]['type'] == 'auto') {
            $soal = $db->query("SELECT * FROM `t_soal` WHERE `id_kelas` = '{$kelas}' AND `kode_mapel` = '{$query[0]['kode_mapel']}' AND `id_soal` NOT IN (SELECT `list_soal` as `soal` FROM `t_cat` WHERE `id_ujian`='{$_SESSION['id_ujian']}'  AND `nomor_induk_siswa`='{$nik}') ORDER BY RAND() LIMIT 1");
          } else {
            $soal = $db->query("SELECT * FROM `t_soal` WHERE `kode_mapel` = '{$query[0]['kode_mapel']}' AND `id_soal` NOT IN (SELECT `list_soal` as `soal` FROM `t_cat` WHERE `id_ujian`='{$_SESSION['id_ujian']}' AND `nomor_induk_siswa`='{$nik}') ORDER BY `id_soal` ASC LIMIT 1");
          }


        } else {

          //membuat validasi next setelah back
          $_SESSION['page']=$_SESSION['page']-1;
          $soal = $db->query("SELECT `t_cat`.`list_jawaban`,`t_soal`.`id_soal`,`t_soal`.`soal`,`t_soal`.`opsi_a`,`t_soal`.`opsi_b`,`t_soal`.`opsi_c`,`t_soal`.`opsi_d`,`t_soal`.`opsi_e`,`id_kelas` FROM `t_cat`
        LEFT JOIN `t_soal` ON `t_cat`.`list_soal` = `t_soal`.`id_soal`
        WHERE `id_kelas` = '{$kelas}' AND `id_ujian`='{$_SESSION['id_ujian']}' AND `nomor_induk_siswa`='{$nik}' LIMIT {$_SESSION['page']}, 1");

        }

        foreach ($soal as $value) {
            ?>
            <?php echo $value['soal'];?>
            <form method="post" name="form_soal" id="soal">
              <input type="hidden" name="soal" value="<?php echo $value['id_soal'];?>">
              <div class="radio">
                <label>
                  <?php
                    if (@$value['list_jawaban'] == 'a') {
                      echo '<input type="radio" id="jawab" name="jawaban" value="a" checked>' . $value['opsi_a'];
                    } else {
                      echo '<input type="radio" id="jawab" name="jawaban" value="a">' . $value['opsi_a'];
                    }
                  ?>
                </label>
              </div>

              <div class="radio">
                <label>
                  <?php
                    if (@$value['list_jawaban'] == 'b') {
                      echo '<input type="radio" id="jawab" name="jawaban" value="b" checked>' . $value['opsi_b'];
                    } else {
                      echo '<input type="radio" id="jawab" name="jawaban" value="b">' . $value['opsi_b'];
                    }
                  ?>
                </label>
              </div>

              <div class="radio">
                <label>
                  <?php
                    if (@$value['list_jawaban'] == 'c') {
                      echo '<input type="radio" id="jawab" name="jawaban" value="c" checked>' . $value['opsi_c'];
                    } else {
                      echo '<input type="radio" id="jawab" name="jawaban" value="c">' . $value['opsi_c'];
                    }
                  ?>
                </label>
              </div>

              <div class="radio">
                <label>
                  <?php
                    if (@$value['list_jawaban'] == 'd') {
                      echo '<input type="radio" id="jawab" name="jawaban" value="d" checked>' . $value['opsi_d'];
                    } else {
                      echo '<input type="radio" id="jawab" name="jawaban" value="d">' . $value['opsi_d'];
                    }
                  ?>
                </label>
              </div>

              <div class="radio">
                <label>
                  <?php
                    if (@$value['list_jawaban'] == 'e') {
                      echo '<input type="radio" id="jawab" name="jawaban" value="e" checked>' . $value['opsi_e'];
                    } else {
                      echo '<input type="radio" id="jawab" name="jawaban" value="e">' . $value['opsi_e'];
                    }
                  ?>
                </label>
              </div>


              <div class="col-md-2">
                <?php 
                if(!empty($dijawab)) {
                  echo '<a href="#" class="btn btn-default" id="back">Back</a>';
                }
              ?>
                <a href="#" class="btn btn-primary" id="next">Next</a>
              </div>

              <div class="col-md-offset-8 col-md-2">
                <strong><?php echo $dijawab;?></strong> / <?php echo $query[0]['jumlah_soal'];?>
              </div>
            </form>
            <?php
        }
      }
      
    } else {

      $soal = $db->query("SELECT `t_cat`.`id_cat`,`t_cat`.`id_ujian`,`t_cat`.`nomor_induk_siswa`,`t_cat`.`list_soal`,`t_soal`.`id_soal`,`t_cat`.`list_jawaban`,`t_soal`.`soal`,`t_soal`.`opsi_a`,`t_soal`.`opsi_b`,`t_soal`.`opsi_c`,`t_soal`.`opsi_d`,`t_soal`.`opsi_e`,`id_kelas` FROM `t_cat` LEFT JOIN `t_soal` ON `t_cat`.`list_soal` = `t_soal`.`id_soal` WHERE `id_kelas` = '{$kelas}' AND `t_cat`.`id_ujian` = '{$id_ujian}' AND `t_cat`.`nomor_induk_siswa`='{$nik}'");

    ?>

      <div class="col-md-12 alert alert-info" role="alert">
        <p><strong>Perhatian</strong> : sebelum mengirim hasil final ujian kamu, harap periksa kembali jawaban Anda. Terima kasih!</p>
      </div>
      <br />
      <br />
      <br />

      <?php
      $no=0;
      foreach ($soal as $value) {
        $no++;
            ?>
            <form method="post" id="all">
              
              <div class="panel panel-default">
                <div class="panel-title">
                  <div class="panel-body">
                    <?php echo '<span style="float: left; margin-right: 20px">' . $no . '. </span>' . $value['soal'];?>
                  </div>
                </div>
                <hr />
              <div class="radio panel-body">
                <label class="col-md-4">
                  <?php
                  if ($value['list_jawaban'] == 'a') {
                    echo '<input type="radio" id="jwb" name="jawaban" value="a" checked id-data="' . $value['id_cat'] . '">' . $value['opsi_a'];
                  } else {
                    echo '<input type="radio" id="jwb" name="jawaban" value="a" id-data="' . $value['id_cat'] . '">' . $value['opsi_a'];
                  }
                  ?>
                </label>

                <label class="col-md-4">
                  <?php
                  if ($value['list_jawaban'] == 'b') {
                    echo '<input type="radio" id="jwb" name="jawaban" value="b" checked id-data="' . $value['id_cat'] . '">' . $value['opsi_b'];
                  } else {
                    echo '<input type="radio" id="jwb" name="jawaban" value="b" id-data="' . $value['id_cat'] . '">' . $value['opsi_b'];
                  }
                  ?>
                </label>
              </div>

              <div class="radio panel-body">
                <label class="col-md-4">
                  <?php
                  if ($value['list_jawaban'] == 'c') {
                    echo '<input type="radio" id="jwb" name="jawaban" value="c" checked id-data="' . $value['id_cat'] . '">' . $value['opsi_c'];
                  } else {
                    echo '<input type="radio" id="jwb" name="jawaban" value="c" id-data="' . $value['id_cat'] . '">' . $value['opsi_c'];
                  }
                  ?>
                </label>

                <label class="col-md-4">
                  <?php
                  if ($value['list_jawaban'] == 'd') {
                    echo '<input type="radio" id="jwb" name="jawaban" value="d" checked id-data="' . $value['id_cat'] . '">' . $value['opsi_d'];
                  } else {
                    echo '<input type="radio" id="jwb" name="jawaban" value="d" id-data="' . $value['id_cat'] . '">' . $value['opsi_d'];
                  }
                  ?>
                </label>
              </div>

              <div class="radio panel-body">
                <label class="col-md-4">
                  <?php
                  if ($value['list_jawaban'] == 'e') {
                    echo '<input type="radio" id="jwb" name="jawaban" value="e" checked id-data="' . $value['id_cat'] . '">' . $value['opsi_e'];
                  } else {
                    echo '<input type="radio" id="jwb" name="jawaban" value="e" id-data="' . $value['id_cat'] . '">' . $value['opsi_e'];
                  }
                  ?>
                </label>
              </div>

              </div>
              <hr />
            </form>
            <?php
      }

      ?>

      <div class="col-md-4 col-md-offset-4">
        <button id="kirim" class="btn btn-primary btn-lg">Kirim Jawaban</button>
      </div>
      <?php
    }
    
?>