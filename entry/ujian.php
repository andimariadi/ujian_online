<?php
require '../system/function.php';
$db = new crud();

//hak akses
$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if (($res[0]['level'] != 'guru') AND ($res[0]['level'] != 'admin')) {
  header('location: ' . base_url('dist/index.php'));
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Input Ujian Siswa - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
  <?php


if (isset($_POST['simpan'])) {
  $nama               = mysqli_real_escape_string($db->connection, $_POST['ujian']);
  $nip                = mysqli_real_escape_string($db->connection, $_POST['nip']);
  $mapel              = mysqli_real_escape_string($db->connection, $_POST['mapel']);
  $kelas              = mysqli_real_escape_string($db->connection, $_POST['kelas']);
  $j_soal             = mysqli_real_escape_string($db->connection, $_POST['j_soal']);
  $type               = mysqli_real_escape_string($db->connection, $_POST['type']);
  $jam                = mysqli_real_escape_string($db->connection, $_POST['jam'] . ':00');
  $tanggal            = mysqli_real_escape_string($db->connection, $_POST['thn'] . '-' . $_POST['bln'] . '-' . $_POST['tgl']);
  $lama               = mysqli_real_escape_string($db->connection, $_POST['lama']);
  $token              = mysqli_real_escape_string($db->connection, strtoupper(substr(md5(date('now').time('now')), 0, 5)) );

  //check status jumlah artikel
  $cek_soal           = $db->query("SELECT * FROM `t_soal` WHERE `kode_mapel`='$mapel' AND `id_kelas`='$kelas'");

  if (mysqli_num_rows($cek_soal) >= $j_soal) {

    $simpan = $db->insert('t_ujian', 
      array(
        'nama_ujian' => $nama,
        'nip_guru' => $nip, 
        'kode_mapel' => $mapel, 
        'id_kelas' => $kelas, 
        'jumlah_soal' => $j_soal, 
        'jam_ujian' => $jam,
        'tanggal' => $tanggal,
        'waktu_ujian' => $lama,
        'type' => $type,
        'token' => $token,
      ));

    if (empty($simpan)) {
      echo '<script type="text/javascript">
        swal({
        title: "Great!",
        text: "Data berhasil disimpan!",
        type: "success",
        confirmButtonText: "Oke"
      });</script>';
    } else {
      echo '<script type="text/javascript">
        swal({
        title: "Error!",
        text: "Tampaknya ada kesalahan proses menyimpan",
        type: "error",
        confirmButtonText: "Oke"
      });</script>';
    }

  } else {
    echo '<script type="text/javascript">
        swal({
        title: "Error!",
        text: "Soal tidak cukup!",
        type: "error",
        confirmButtonText: "Oke"
      });</script>';
  }

  
  
}

?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
        <h1>Input Ujian Siswa</h1>
        <div class="alert alert-info" role="alert">
          <a href="<?php echo base_url('data/ujian.php');?>" class="btn btn-default"><span class="glyphicon glyphicon-menu-left"></span> Kembali</a>
        </div>

        <form method="post" class="form-horizontal">

          <div class="page-header">
            <h3>Buat Ujian Siswa <?php echo date('d/m/Y H:m');?></h3>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Nama Ujian</label>
            <div class="col-sm-3">
              <input type="text" name="ujian" class="form-control" placeholder="Nama Ujian Siswa">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">NIP / Nama</label>
            <div class="col-sm-5">
              <select class="form-control" name="nip"  id="nip">
                <?php
                if ($res[0]['level'] == 'guru') {
                  $cek_nip = substr($res[0]['username'], 2);
                  $cek_guru = $db->where('t_guru', array('nip' => $cek_nip));
                  foreach ($cek_guru as $value) {
                    echo '<option value="' . $value['nip'] . '">' . $value['nip'] . ' / ' . $value['nama'] . '</option>';
                  }
                } elseif ($res[0]['level'] == 'admin'){
                  $cek_guru = $db->view('t_guru');
                  foreach ($cek_guru as $value) {
                    echo '<option value="' . $value['nip'] . '">' . $value['nip'] . ' / ' . $value['nama'] . '</option>';
                  }
                }

                ?>
              </select>
            </div>

            <label class="col-sm-1 control-label">MAPEL</label>
            <div class="col-sm-4">
              <select class="form-control" name="mapel" id="mapel">
                <option value=""></option>
              </select>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 control-label">Kelas</label>
            <div class="col-md-3">
              <select class="form-control" name="kelas">
              <option>Pilih kelas</option>
                <?php
                  $cek_guru = $db->view('t_kelas');
                  foreach ($cek_guru as $value) {
                    echo '<option value="' . $value['id_kelas'] . '">' . $value['kelas'] . '</option>';
                  }
                ?>
            </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Jumlah Soal</label>
            <div class="col-sm-2">
              <input type="text" name="j_soal" class="form-control" placeholder="Jumlah soal">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Type Soal</label>
            <div class="col-sm-4">
              <select class="form-control" name="type" id="type">
                <option value="auto">Soal Acak</option>
                <option value="urut">Soal Berurutan</option>
              </select>
            </div>
            <div id="after_type"></div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Jam / Tanggal</label>

            <div class="col-sm-2">
              <input type="text" name="jam" class="form-control" placeholder="HH:MM">
            </div>

            <div class="col-sm-1 col-xs-4">
            <select class="form-control" name="tgl">
              <?php
                for ($i=1; $i <= 31 ; $i++) { 
                  echo '<option value="' . $i . '">' . $i . '</option>';
                }
              ?>
            </select>
            </div>

            <div class="col-sm-1 col-xs-4">
            <select class="form-control" name="bln">
              <?php
                for ($i=1; $i <= 12 ; $i++) { 
                  echo '<option value="' . $i . '">' . $i . '</option>';
                }
              ?>
            </select>
            </div>
            
            <div class="col-sm-2 col-xs-4">
            <select class="form-control" name="thn">
              <?php
                for ($i=date('Y'); $i >= 2000 ; $i--) { 
                  echo '<option value="' . $i . '">' . $i . '</option>';
                }
              ?>
            </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-2 col-xs-12 control-label">Lama Ujian</label>
            <div class="col-sm-2 col-xs-4">
              <input type="text" name="lama" class="form-control" placeholder="60">
            </div>
              <p class="form-control-static  control-label" style="float: left;">menit</p>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-4">
              <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
              <button type="submit" name="simpan" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
          </div>
          

        </form>
        


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>
    <script type="text/javascript">
    $(document).ready(function() {
        var anchor = window.location.hash;
        $(".collapse").collapse('hide');
        $('#collapseFive').collapse('show');
        var nip = $('#nip').val();
        $.ajax({
          url: '<?php echo base_url('data/api/mapel.php');?>',
          data: 'nip='+nip ,
          success:function(data) {
            $("#mapel").html(data);
          }
        })
        $('#nip').on('change', function(e) {
          var nip = $('#nip').val();
          $.ajax({
            url: '<?php echo base_url('data/api/mapel.php');?>',
            data: 'nip='+nip,
            success:function(data) {
              $("#mapel").html(data);
            },
            error:function(data) {
              alert('taik');
            }
          })
        })
    });
    tinymce.init({
  selector: 'textarea',
  
  plugins: 'image, table'
});
    </script>
  </body>
</html>