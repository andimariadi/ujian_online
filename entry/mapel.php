<?php
require '../system/function.php';
$db = new crud();

//hak akses
$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if ($res[0]['level'] != 'admin') {
  header('location: ' . base_url('dist/index.php'));
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Input Mata Pelajaran - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
  <?php

if (isset($_POST['simpan'])) {
  $nama               = mysqli_real_escape_string($db->connection, ucwords($_POST['nama']));
  $kode               = kode($nama);

  //create number
  $cek_kode           = $db->query("SELECT * FROM `t_mapel` WHERE `kode_mapel` LIKE '$kode%' ORDER BY `t_mapel`.`kode_mapel` DESC ");
  $cek_number_terakhir= $cek_kode->fetch_all(MYSQLI_ASSOC);
  if (mysqli_num_rows($cek_kode) != 0) {
    $kode               = $kode . '-' . autonum(substr(($cek_number_terakhir[0]['kode_mapel']), 3));
  } else {
    $kode               = $kode . '-' . autonum(mysqli_num_rows($cek_kode));
  }
  
  //insert
  $simpan = $db->insert('t_mapel', 
      array(
        'kode_mapel' => $kode, 
        'nama_mapel' => $nama
      ));

  if (empty($simpan)) {
      echo '<script type="text/javascript">
        swal({
        title: "Great!",
        text: "Data berhasil disimpan!",
        type: "success",
        confirmButtonText: "Oke"
      });</script>';
  } else {
      echo '<script type="text/javascript">
        swal({
        title: "Error!",
        text: "Tampaknya ada kesalahan proses menyimpan",
        type: "error",
        confirmButtonText: "Oke"
      });</script>';
  }
  
}

?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
        <h1>Input Kelas</h1>
        <div class="alert alert-info" role="alert">
          <a href="<?php echo base_url('data/mapel.php');?>" class="btn btn-default"><span class="glyphicon glyphicon-menu-left"></span> Kembali</a>
        </div>

        <form method="post" class="form-horizontal">

          <div class="page-header">
            <h3>Data Mata Pelajaran</h3>
          </div>
          
          <div class="form-group">
            <label class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-5">
              <input type="text" name="nama" class="form-control" placeholder="Nama Mata Pelajaran">
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-4">
              <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
              <button type="submit" name="simpan" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
          </div>
          

        </form>
        


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>
    <script type="text/javascript">
    $(document).ready(function() {
        $(".collapse").collapse('hide');
        $('#collapseThree').collapse('show');
    });
    </script>
  </body>
</html>