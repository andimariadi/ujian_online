<?php
require '../system/function.php';
$db = new crud();

//hak akses
if (empty($_SESSION['username'])) {
  header('location: ' . base_url('dist/index.php'));
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ganti Password - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
  <?php

if (isset($_POST['simpan'])) {
  $slama                 = mysqli_real_escape_string($db->connection, $_POST['slama']);
  $sbaru                 = mysqli_real_escape_string($db->connection, $_POST['sbaru']);
  $ubaru                 = mysqli_real_escape_string($db->connection, $_POST['ubaru']);
  $cek                   = $db->where('t_user', array('username' => $_SESSION['username']));
  $data                  = $cek->fetch_all(MYSQLI_ASSOC);

  if (mysqli_num_rows($cek) > 0) {
    if (password_verify($slama, $data[0]['password'])) {
      if ($sbaru == $ubaru) {
        $password = PASSWORD_HASH($sbaru, PASSWORD_DEFAULT);
        $simpan   = $db->update('t_user', array('password' => $password), array('username' => $_SESSION['username']));
        if (!$simpan) {
          echo '<script type="text/javascript">
            swal({
            title: "Great!",
            text: "Password berhasil diganti! Silahkan login kembali",
            type: "success",
            confirmButtonText: "Oke"
          },
            function() {
              window.location.assign(\'' . base_url('logout.php') . '\')
            });</script>';
        }
      } else {
        echo '<script type="text/javascript">
          swal({
          title: "Error!",
          text: "Password baru tidak sesuai",
          type: "error",
          confirmButtonText: "Oke"
        });</script>';
      }
    } else {
      echo '<script type="text/javascript">
      swal({
      title: "Error!",
      text: "Password salah!",
      type: "error",
      confirmButtonText: "Oke"
    });</script>';
    }
  }
  

  /*

  $cek = $db->where('t_guru', array('nip' => $nip));
  if (mysqli_num_rows($cek) > 0) {
    echo '<script type="text/javascript">
      swal({
      title: "Error!",
      text: "Nomor Induk Pegawai ' . $nip . ' sudah ada!",
      type: "error",
      confirmButtonText: "Oke"
    });</script>';
  } else {
    $simpan = $db->insert('t_guru', 
      array(
        'nip' => $nip, 
        'nama' => $nama, 
        'alamat' => $alamat, 
        'telp' => $telp
      ));

    //insert to tb_user untuk login
    $password = PASSWORD_HASH($nip, PASSWORD_DEFAULT);
    $db->insert('t_user', array('username' => 'G-' . $nip, 'password' => $password, 'level' => 'guru'));
    //end insert
    if (empty($simpan)) {
      echo '<script type="text/javascript">
        swal({
        title: "Great!",
        text: "Data berhasil disimpan!",
        type: "success",
        confirmButtonText: "Oke"
      });</script>';
    } else {
      echo '<script type="text/javascript">
        swal({
        title: "Error!",
        text: "Tampaknya ada kesalahan proses menyimpan",
        type: "error",
        confirmButtonText: "Oke"
      });</script>';
    }
  
  }
  */
  
}

?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">

        <form method="post" class="form-horizontal">

          <div class="page-header">
            <h3>Ganti Sandi</h3>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Sandi Lama</label>
            <div class="col-sm-4">
              <input type="password" name="slama" class="form-control" placeholder="Masukkan sandi lama">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Sandi Baru</label>
            <div class="col-sm-4">
              <input type="password" name="sbaru" class="form-control" placeholder="Masukkan sandi baru">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Ulangi Sandi</label>
            <div class="col-sm-4">
              <input type="password" name="ubaru" class="form-control" placeholder="Ulangi sandi baru">
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-4">
              <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
              <button type="submit" name="simpan" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
          </div>
          

        </form>
        


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>
    <script type="text/javascript">
    
    $(document).ready(function() {
        var anchor = window.location.hash;
        $(".collapse").collapse('hide');
        $('#collapseTwo').collapse('show');
    });
    </script>
  </body>
</html>