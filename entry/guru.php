<?php
require '../system/function.php';
$db = new crud();

//hak akses
$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if ($res[0]['level'] != 'admin') {
  header('location: ' . base_url('dist/index.php'));
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Input Data Guru - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
  <?php

if (isset($_POST['simpan_guru'])) {
  $nip                = mysqli_real_escape_string($db->connection, $_POST['nip']);
  $nama               = mysqli_real_escape_string($db->connection, $_POST['nama']);
  $alamat             = mysqli_real_escape_string($db->connection, $_POST['alamat']);
  $telp               = mysqli_real_escape_string($db->connection,  '+62' . $_POST['telp']);

  $cek = $db->where('t_guru', array('nip' => $nip));
  if (mysqli_num_rows($cek) > 0) {
    echo '<script type="text/javascript">
      swal({
      title: "Error!",
      text: "Nomor Induk Pegawai ' . $nip . ' sudah ada!",
      type: "error",
      confirmButtonText: "Oke"
    });</script>';
  } else {
    $simpan = $db->insert('t_guru', 
      array(
        'nip' => $nip, 
        'nama' => $nama, 
        'alamat' => $alamat, 
        'telp' => $telp
      ));

    //insert to tb_user untuk login
    $password = PASSWORD_HASH($nip, PASSWORD_DEFAULT);
    $db->insert('t_user', array('username' => 'G-' . $nip, 'password' => $password, 'level' => 'guru'));
    //end insert
    if (empty($simpan)) {
      echo '<script type="text/javascript">
        swal({
        title: "Great!",
        text: "Data berhasil disimpan!",
        type: "success",
        confirmButtonText: "Oke"
      });</script>';
    } else {
      echo '<script type="text/javascript">
        swal({
        title: "Error!",
        text: "Tampaknya ada kesalahan proses menyimpan",
        type: "error",
        confirmButtonText: "Oke"
      });</script>';
    }
  
  }
  
}

?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
        <h1>Input Data Guru</h1>
        <div class="alert alert-info" role="alert">
          <a href="<?php echo base_url('data/guru.php');?>" class="btn btn-default"><span class="glyphicon glyphicon-menu-left"></span> Kembali</a>
        </div>

        <form method="post" class="form-horizontal">

          <div class="page-header">
            <h3>Data Guru</h3>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">NIP</label>
            <div class="col-sm-3">
              <input type="text" name="nip" class="form-control" placeholder="Nomor Induk Pegawai">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-5">
              <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Alamat</label>
            <div class="col-sm-6">
              <textarea  name="alamat" class="form-control" rows="3" placeholder="Alamat Lengkap"></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">No Telp.</label>
            <div class="col-xs-1">
              <p class="form-control-static  control-label">+62</p>
            </div>
            <div class="col-sm-4">
              <input type="text" name="telp" class="form-control" placeholder="81234567890">
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-4">
              <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
              <button type="submit" name="simpan_guru" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
          </div>
          

        </form>
        


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>
    <script type="text/javascript">
    
    $(document).ready(function() {
        var anchor = window.location.hash;
        $(".collapse").collapse('hide');
        $('#collapseTwo').collapse('show');
    });
    </script>
  </body>
</html>