<?php
require '../system/function.php';
  $db = new crud();
	if (isset($_POST['soal'])) {
		$id_ujian	= mysqli_real_escape_string($db->connection, $_SESSION['id_ujian']);
		$nis		= mysqli_real_escape_string($db->connection, substr($_SESSION['username'], 2));
		$id_soal	= mysqli_real_escape_string($db->connection, $_POST['soal']);
		$jawaban	= mysqli_real_escape_string($db->connection, $_POST['jawaban']);
		//cari dulu id_cat
		$cek = $db->query("SELECT * FROM `t_cat` WHERE `id_ujian`='{$id_ujian}' AND `nomor_induk_siswa`='{$nis}' AND `list_soal`='{$id_soal}'");
		if ($cek->num_rows > 0) {
			$insert = $db->query("
				UPDATE `t_cat` SET `list_jawaban`='{$jawaban}' WHERE `id_ujian`='{$id_ujian}' AND `nomor_induk_siswa`='{$nis}' AND `list_soal`='{$id_soal}'
				");
		} else {
			$insert = $db->insert('t_cat',
				array(
					'id_ujian' => $id_ujian,
					'nomor_induk_siswa' => $nis,
					'list_soal' => $id_soal, 
					'list_jawaban' => $jawaban
				)
			);
		}
	}

	if (!empty($_GET['s']) == 'update') {
		$jwb = mysqli_real_escape_string($db->connection, $_POST['jwb']);
		$idcat = mysqli_real_escape_string($db->connection, $_POST['id_cat']);
		$update = $db->update('t_cat', array('list_jawaban' => $jwb), array('id_cat' => $idcat));
	}
?>