<?php require '../system/function.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Index Ujian</title>
<?php require '../include/head.php';?>
  </head>

  <body class="login-body">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4 white-body">
        <div class="panel panel-primary row login-header">
          <div class="panel-heading">
            <h1><span class="glyphicon glyphicon-info-sign"></span> Informasi Sistem</h1>
          </div>
        </div>
        <p>Tampaknya halaman yang diakses tidak tersedia.</p>
        <p class="text-center"><a href="<?php echo base_url('/index.php');?>" class="btn btn-primary">Home</a>.</p>
      </div>
    </div>
  </div>

    
  </body>
</html>
