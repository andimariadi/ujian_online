<?php
require '../system/function.php';
$db = new crud();

//hak akses
$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if ($res[0]['level'] != 'admin') {
  header('location: ' . base_url('dist/index.php'));
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Input Data Guru - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
  <?php

if (isset($_POST['simpan'])) {
  $nip                = mysqli_real_escape_string($db->connection, $_POST['nip']);
  $mapel              = $_POST['mapel'];
  $i_mapel            = implode(',', $mapel);

  $cek = $db->where('t_guru_mapel', array('nip_guru' => $nip));

  if (mysqli_num_rows($cek) > 0) {
    echo '<script type="text/javascript">
        swal({
        title: "Error!",
        text: "Guru dengan NIP ini sudah ada!",
        type: "error",
        confirmButtonText: "Oke"
      });</script>';
  } else {
    $cek = $db->where('t_guru', array('nip' => $nip));
    if (mysqli_num_rows($cek) > 0) {
        $simpan = $db->insert('t_guru_mapel', 
        array(
          'nip_guru' => $nip, 
          'no_mapel' => $i_mapel
        ));

        echo '<script type="text/javascript">
          swal({
          title: "Great!",
          text: "Data berhasil disimpan!",
          type: "success",
          confirmButtonText: "Oke"
        });</script>';
      
    } else {
      echo '<script type="text/javascript">
          swal({
          title: "Error!",
          text: "NIP tidak terdapat pada sistem",
          type: "error",
          confirmButtonText: "Oke"
        });</script>';
      
    }
  }
  
}

?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
        <h1>Input Guru Mata Pelajaran</h1>
        <div class="alert alert-info" role="alert">
          <a href="<?php echo base_url('data/guru_mapel.php');?>" class="btn btn-default"><span class="glyphicon glyphicon-menu-left"></span> Kembali</a>
        </div>

        <form method="post" class="form-horizontal">

          <div class="page-header">
            <h3>Data Guru Mapel</h3>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">NIP / Nama</label>
            <div class="col-sm-5">
              <select class="form-control" name="nip">
              <?php
                $mapel = $db->view('t_guru');
                foreach ($mapel as $element) {?>
                  <option value="<?php echo $element['nip'];?>"><?php echo $element['nip'];?> / <?php echo $element['nama'];?></option>
              <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Mata Pelajaran</label>
            <div class="col-sm-10">
            <?php
            $mapel = $db->view('t_mapel');
            foreach ($mapel as $element) {?>
              <label class="checkbox-inline">
                <input type="checkbox" name="mapel[]" value="<?php echo $element['kode_mapel'];?>"> <?php echo $element['nama_mapel'];?>
              </label>
            <?php } ?>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-4">
              <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
              <button type="submit" name="simpan" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
          </div>
          

        </form>
        


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>
    <script type="text/javascript">
    $(document).ready(function() {
        var anchor = window.location.hash;
        $(".collapse").collapse('hide');
        $('#collapseTwo').collapse('show');
    });
    </script>
  </body>
</html>