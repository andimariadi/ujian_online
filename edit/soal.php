<?php
require '../system/function.php';
$db = new crud();

//get data id
$no = mysqli_real_escape_string($db->connection, $_GET['no']);

//hak akses
$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if (($res[0]['level'] == 'murid') or empty($_SESSION['username']) or (empty($no))) {
  header('location: ' . base_url('dist/index.php'));
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Input Soal Guru - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
  <?php

if (isset($_POST['simpan'])) {
  $nip                  = mysqli_real_escape_string($db->connection, $_POST['nip']);
  $mapel                = mysqli_real_escape_string($db->connection, $_POST['mapel']);
  $kelas                = mysqli_real_escape_string($db->connection, $_POST['kelas']);
  $soal                 = mysqli_real_escape_string($db->connection, $_POST['soal']);
  $a                    = mysqli_real_escape_string($db->connection, $_POST['a']);
  $b                    = mysqli_real_escape_string($db->connection, $_POST['b']);
  $c                    = mysqli_real_escape_string($db->connection, $_POST['c']);
  $d                    = mysqli_real_escape_string($db->connection, $_POST['d']);
  $e                    = mysqli_real_escape_string($db->connection, $_POST['e']);
  $jawaban              = mysqli_real_escape_string($db->connection, $_POST['jawaban']);
  if (empty($nip) || empty($mapel) || empty($soal) || empty($a) || empty($b) || empty($c) || empty($d) || empty($e) || empty($jawaban)) {
    echo '<script type="text/javascript">
        swal({
        title: "Error!",
        text: "Field tidak boleh kosong!",
        type: "error",
        confirmButtonText: "Oke"
      });</script>';
  } else {
    $simpan = $db->update('t_soal', 
      array(
        'nip_guru' => $nip, 
        'kode_mapel' => $mapel, 
        'id_kelas' => $kelas, 
        'soal' => $soal, 
        'opsi_a' => $a,
        'opsi_b' => $b,
        'opsi_c' => $c,
        'opsi_d' => $d,
        'opsi_e' => $e,
        'jawaban' => $jawaban
      ), array(
        'id_soal' => $no
      ));

    //end insert
    if (empty($simpan)) {
      echo '<script type="text/javascript">
        swal({
        title: "Great!",
        text: "Data berhasil disimpan!",
        type: "success",
        confirmButtonText: "Oke"
      });</script>';
    } else {
      echo '<script type="text/javascript">
        swal({
        title: "Error!",
        text: "Tampaknya ada kesalahan proses menyimpan",
        type: "error",
        confirmButtonText: "Oke"
      });</script>';
    }
  
  }
  
}

?>
<?php
  $data       = $db->where('t_soal', array('id_soal' => $no));
  foreach ($data as $values):
?>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
        <h1>Input Soal</h1>
        <div class="alert alert-info" role="alert">
          <a href="<?php echo base_url('data/soal.php');?>" class="btn btn-default"><span class="glyphicon glyphicon-menu-left"></span> Kembali</a>
        </div>

        <form method="post">

        
          <div class="page-header">
            <h3>Bank Soal</h3>
          </div>
          <div class="form-group row">
            <label class="col-md-2 control-label">NIP / Nama</label>
            <div class="col-md-4">
              <select class="form-control" name="nip"  id="nip">
                <?php
                if ($res[0]['level'] == 'guru') {
                  $cek_nip = substr($res[0]['username'], 2);
                  $cek_guru = $db->where('t_guru', array('nip' => $cek_nip));
                  foreach ($cek_guru as $value) {
                    if ($value['nip'] == $values['nip_guru']) {
                      echo '<option value="' . $value['nip'] . '" selected>' . $value['nip'] . ' / ' . $value['nama'] . '</option>';
                    } else {
                      echo '<option value="' . $value['nip'] . '">' . $value['nip'] . ' / ' . $value['nama'] . '</option>';
                    }
                  }
                } elseif ($res[0]['level'] == 'admin'){
                  $cek_guru = $db->view('t_guru');
                  foreach ($cek_guru as $value) {
                    if ($value['nip'] == $values['nip_guru']) {
                      echo '<option value="' . $value['nip'] . '" selected>' . $value['nip'] . ' / ' . $value['nama'] . '</option>';
                    } else {
                      echo '<option value="' . $value['nip'] . '">' . $value['nip'] . ' / ' . $value['nama'] . '</option>';
                    }
                  }
                }

                ?>
            </select>
            </div>
            <label class="col-md-2 control-label">Mata Pelajaran</label>
            <div class="col-md-4">
              <select class="form-control" name="mapel" id="mapel">
                <option value=""></option>
              </select>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 control-label">Kelas</label>
            <div class="col-md-3">
              <select class="form-control" name="kelas">
              <option>Pilih kelas</option>
                <?php
                  $cek_guru = $db->view('t_kelas');
                  foreach ($cek_guru as $value) {
                    if ($values['id_kelas'] == $value['id_kelas']) {
                      echo '<option value="' . $value['id_kelas'] . '" selected>' . $value['kelas'] . '</option>';
                    } else {
                      echo '<option value="' . $value['id_kelas'] . '">' . $value['kelas'] . '</option>';
                    }
                    
                  }
                ?>
            </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label">Text Soal</label>
            <textarea class="form-control" rows="5" name="soal" placeholder="Tuliskan soal"><?php echo $values['soal'];?></textarea>
          </div>

          <div class="form-group">
            <label class="control-label">Jawaban A</label>
            <textarea class="form-control" rows="5" name="a" placeholder="Tuliskan soal"><?php echo $values['opsi_a'];?></textarea>
          </div>

          <div class="form-group">
            <label class="control-label">Jawaban B</label>
            <textarea class="form-control" rows="5" name="b" placeholder="Tuliskan soal"><?php echo $values['opsi_b'];?></textarea>
          </div>

          <div class="form-group">
            <label class="control-label">Jawaban C</label>
            <textarea class="form-control" rows="5" name="c" placeholder="Tuliskan soal"><?php echo $values['opsi_c'];?></textarea>
          </div>

          <div class="form-group">
            <label class="control-label">Jawaban D</label>
            <textarea class="form-control" rows="5" name="d" placeholder="Tuliskan soal"><?php echo $values['opsi_d'];?></textarea>
          </div>

          <div class="form-group">
            <label class="control-label">Jawaban E</label>
            <textarea class="form-control" rows="5" name="e" placeholder="Tuliskan soal"><?php echo $values['opsi_e'];?></textarea>
          </div>

          <div class="form-group row">
            <label class="col-md-2 control-label">Jawaban Benar</label>
            <div class="col-md-3">
              <select class="form-control" name="jawaban">
              <?php
                if ($values['jawaban'] == 'a') {
                  echo '<option value="a" selected>A</option>
                  <option value="b">B</option>
                  <option value="c">C</option>
                  <option value="d">D</option>
                  <option value="e">E</option>';
                } elseif ($values['jawaban'] == 'b') {
                  echo '
                  <option value="a">A</option>
                  <option value="b" selected>B</option>
                  <option value="c">C</option>
                  <option value="d">D</option>
                  <option value="e">E</option>';
                } elseif ($values['jawaban'] == 'c') {
                  echo '
                  <option value="a">A</option>
                  <option value="b">B</option>
                  <option value="c" selected>C</option>
                  <option value="d">D</option>
                  <option value="e">E</option>';
                } elseif ($values['jawaban'] == 'd') {
                  echo '
                  <option value="a">A</option>
                  <option value="b">B</option>
                  <option value="c">C</option>
                  <option value="d" selected>D</option>
                  <option value="e">E</option>';
                } else {
                  echo '
                  <option value="a">A</option>
                  <option value="b">B</option>
                  <option value="c">C</option>
                  <option value="d">D</option>
                  <option value="e" selected>E</option>';
                }
              ?>
                
              </select>
            </div>
            <p>* Pastikan jawaban benar sudah diisi.</p>
          </div>


          <div class="form-group">
              <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
              <button type="submit" name="simpan" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div>
          
          
        </form>
        


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>
    <script type="text/javascript">
    $(document).ready(function() {
        var anchor = window.location.hash;
        $(".collapse").collapse('hide');
        $('#collapseFour').collapse('show');
        var nip = $('#nip').val();
        $.ajax({
          url: '<?php echo base_url('data/api/mapel.php');?>',
          data: 'nip='+nip+'&mp=<?php echo $values['kode_mapel'];?>' ,
          success:function(data) {
            $("#mapel").html(data);
          }
        })
        $('#nip').on('change', function(e) {
          var nip = $('#nip').val();
          $.ajax({
            url: '<?php echo base_url('data/api/mapel.php');?>',
            data: 'nip='+nip,
            success:function(data) {
              $("#mapel").html(data);
            },
            error:function(data) {
              alert('taik');
            }
          })
        })
    });
    tinymce.init({
  selector: 'textarea',
  
  plugins: 'image, table'
});
    </script>
    
    <?php endforeach;?>

  </body>
</html>