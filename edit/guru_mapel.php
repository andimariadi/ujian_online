<?php
//error_reporting(0);
require '../system/function.php';
$db = new crud();
$kode = mysqli_real_escape_string($db->connection, $_GET['no']);
//hak akses
$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if (($res[0]['level'] != 'admin') or empty($kode)) {
  header('location: ' . base_url('dist/index.php'));
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Input Data Guru - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
  <?php

if (isset($_POST['simpan'])) {
  $nip                = mysqli_real_escape_string($db->connection, $_POST['nip']);
  $i_mapel            = is_array($_POST['mapel'])? implode(',', $_POST['mapel']): mysqli_real_escape_string($db->connection, $_POST['mapel']);

  
  $cek = $db->where('t_guru', array('nip' => $nip));
  if (mysqli_num_rows($cek) > 0) {
    $simpan = $db->update('t_guru_mapel', 
      array(
          'nip_guru' => $nip, 
          'no_mapel' => $i_mapel
        ),
      array('no' => $kode));

      echo '<script type="text/javascript">
        swal({
        title: "Great!",
        text: "Data berhasil disimpan!",
        type: "success",
        confirmButtonText: "Oke"
      }, function() {
        window.location.assign(\'' . base_url('data/guru_mapel.php') . '\')
      });</script>';
  } else {
      echo '<script type="text/javascript">
        swal({
        title: "Error!",
        text: "Tampaknya ada kesalahan proses menyimpan",
        type: "error",
        confirmButtonText: "Oke"
      });</script>';
  }
}

?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
        <h1>Input Guru Mata Pelajaran</h1>
        <div class="alert alert-info" role="alert">
          <a href="<?php echo base_url('data/guru_mapel.php');?>" class="btn btn-default"><span class="glyphicon glyphicon-menu-left"></span> Kembali</a>
        </div>

        <form method="post" class="form-horizontal">

          <?php
            $data       = $db->where('t_guru_mapel', array('no' => $kode));
            foreach ($data as $value):
          ?>
          <div class="page-header">
            <h3>Data Guru Mapel</h3>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">NIP / Nama</label>
            <div class="col-sm-5">
              <select class="form-control" name="nip">
              <?php

                $mapel = $db->view('t_guru');
                
                foreach ($mapel as $element) {

                  if ($element['nip'] == $value['nip_guru']) {
                    echo '<option value="' . $element['nip'] . '" selected>' . $element['nip'] . '/' . $element['nama'] . 
                    '</option>';
                  } else {
                    echo '<option value="' . $element['nip'] . '">' . $element['nip'] . '/' . $element['nama'] . 
                    '</option>';
                  }
                }
                  ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Mata Pelajaran</label>
            <div class="col-sm-10">
              <input type="hidden" name="mapel">
            <?php
            $ex = explode(',', $value['no_mapel']);
            //print_r($ex);
            $mapel = $db->view('t_mapel');
            foreach ($mapel as $element) {
              echo '<label class="checkbox-inline">';
              echo '<input type="checkbox" name="mapel[]" value="' . $element['kode_mapel'] . '">' . $element['nama_mapel'];
              echo '</label>';
            }
            ?>
              
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-4">
              <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
              <button type="submit" name="simpan" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
          </div>
          
          <?php endforeach;?>

        </form>
        


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>
    <script type="text/javascript">
    $(document).ready(function() {
        var anchor = window.location.hash;
        $(".collapse").collapse('hide');
        $('#collapseTwo').collapse('show');
    });
    </script>
  </body>
</html>