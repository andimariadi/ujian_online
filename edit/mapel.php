<?php
require '../system/function.php';
$db = new crud();
$kode = mysqli_real_escape_string($db->connection, $_GET['no']);
//hak akses
$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if (($res[0]['level'] != 'admin') or empty($kode)) {
  header('location: ' . base_url('dist/index.php'));
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Edit Mata Pelajaran - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
  <?php

//cek nomor apakah ada ?
$ceking                = $db->where('t_mapel', array('kode_mapel' => $kode));
if (empty(mysqli_num_rows($ceking))) {
  echo '<script type="text/javascript">
      swal({
      title: "Error!",
      text: "Nomor tidak ditemukan!",
      type: "error",
      confirmButtonText: "Oke"
    }, function() {
      window.location.assign(\'' . base_url('data/mapel.php') . '\');
    });</script>';
}

if (isset($_POST['simpan'])) {
  $nama               = mysqli_real_escape_string($db->connection, ucwords($_POST['nama']));

  //update
  $simpan = $db->update('t_mapel', 
      array(
        'nama_mapel' => $nama
      ), array(
        'kode_mapel' => $kode
      ));

  if (empty($simpan)) {
      echo '<script type="text/javascript">
        swal({
        title: "Great!",
        text: "Data berhasil disimpan!",
        type: "success",
        confirmButtonText: "Oke"
      }, function() {
        window.location.assign(\'' . base_url('data/mapel.php') . '\')
      });</script>';
  } else {
      echo '<script type="text/javascript">
        swal({
        title: "Error!",
        text: "Tampaknya ada kesalahan proses menyimpan",
        type: "error",
        confirmButtonText: "Oke"
      });</script>';
  }
  
}

?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
        <h1>Edit Mata Pelajaran</h1>
        <div class="alert alert-info" role="alert">
          <a href="<?php echo base_url('data/mapel.php');?>" class="btn btn-default"><span class="glyphicon glyphicon-menu-left"></span> Kembali</a>
        </div>

        <form method="post" class="form-horizontal">

          <?php
            $data       = $db->where('t_mapel', array('kode_mapel' => $kode));
            foreach ($data as $value):
          ?>
          <div class="page-header">
            <h3>Data Guru</h3>
          </div>
          
          <div class="form-group">
            <label class="col-sm-2 control-label">Kode Mata Pelajaran</label>
            <div class="col-sm-5">
              <input type="text" name="kode" class="form-control" placeholder="Kode Mata Pelajaran" disabled="" value="<?php echo $value['kode_mapel'];?>">
            </div>
          </div>


          <div class="form-group">
            <label class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-5">
              <input type="text" name="nama" class="form-control" placeholder="Nama Mata Pelajaran" value="<?php echo $value['nama_mapel'];?>">
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-4">
              <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
              <button type="submit" name="simpan" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
          </div>
          
          <?php endforeach;?>

        </form>
        


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>
    <script type="text/javascript">
    $(document).ready(function() {
        $(".collapse").collapse('hide');
        $('#collapseThree').collapse('show');
    });
    </script>
  </body>
</html>