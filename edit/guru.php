<?php
require '../system/function.php';
$db = new crud();
$no = mysqli_real_escape_string($db->connection, $_GET['no']);

$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if (($res[0]['level'] == 'murid') or empty($no) or (substr($_SESSION['username'], 2) != $no)) {
  header('location: ' . base_url('dist/index.php'));
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Input Data Guru - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
  <?php
//cek nomor apakah ada ?
$ceking                = $db->where('t_guru', array('nip' => $no));
if (empty(mysqli_num_rows($ceking))) {
  echo '<script type="text/javascript">
      swal({
      title: "Error!",
      text: "Nomor induk pegawai tidak ditemukan!",
      type: "error",
      confirmButtonText: "Oke"
    }, function() {
      window.location.assign(\'' . base_url('data/guru.php') . '\');
    });</script>';
}


if (isset($_POST['simpan_guru'])) {
  $nama               = mysqli_real_escape_string($db->connection, $_POST['nama']);
  $alamat             = mysqli_real_escape_string($db->connection, $_POST['alamat']);
  $telp               = mysqli_real_escape_string($db->connection,  '+62' . $_POST['telp']);

// hak akses
  $cek                = $db->where('t_user', array('username' => $_SESSION['username']));
  $cek                = $cek->fetch_all(MYSQLI_ASSOC);

  //hanya sesuai user id atau admin yang dapat edit
  if (($_SESSION['username'] == $no) or ($cek[0]['level'] == 'admin')) {
    $simpan = $db->update('t_guru', 
      array(
        'nama' => $nama, 
        'alamat' => $alamat, 
        'telp' => $telp
      ), array(
        'nip' => $no
      ));
    //end insert
    if (empty($simpan)) {
      echo '<script type="text/javascript">
        swal({
        title: "Great!",
        text: "Data berhasil disimpan!",
        type: "success",
        confirmButtonText: "Oke"
      }, function() {
        window.location.assign(\'' . base_url('data/guru.php') . '\');
      });</script>';
    } else {
      echo '<script type="text/javascript">
        swal({
        title: "Error!",
        text: "Tampaknya ada kesalahan proses menyimpan",
        type: "error",
        confirmButtonText: "Oke"
      }, function() {
        window.location.assign(\'' . base_url('data/guru.php') . '\');
      });</script>';
    }
  } else {
    echo '<script type="text/javascript">
      swal({
      title: "Error!",
      text: "Hak akses ditolak!",
      type: "error",
      confirmButtonText: "Oke"
    }, function() {
      window.location.assign(\'' . base_url('data/guru.php') . '\');
    });</script>';
  }
}
?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
        <h1>Input Data Guru</h1>
        <div class="alert alert-info" role="alert">
          <a href="<?php echo base_url('data/guru.php');?>" class="btn btn-default"><span class="glyphicon glyphicon-menu-left"></span> Kembali</a>
        </div>

        <form method="post" class="form-horizontal">

        <?php
          $data       = $db->where('t_guru', array('nip' => $no));
          foreach ($data as $value):
        ?>
          
          <div class="page-header">
            <h3>Data Guru</h3>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">NIP</label>
            <div class="col-sm-3">
              <input type="text" name="nip" class="form-control" placeholder="Nomor Induk Pegawai" value="<?php echo $value['nip'];?>" disabled>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-5">
              <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" value="<?php echo $value['nama'];?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Alamat</label>
            <div class="col-sm-6">
              <textarea  name="alamat" class="form-control" rows="3" placeholder="Alamat Lengkap"><?php echo $value['alamat'];?></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">No Telp.</label>
            <div class="col-xs-1">
              <p class="form-control-static  control-label">+62</p>
            </div>
            <div class="col-sm-4">
              <input type="text" name="telp" class="form-control" placeholder="81234567890" value="<?php echo substr($value['telp'], 3);?>">
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-4">
              <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
              <button type="submit" name="simpan_guru" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
          </div>
          <?php endforeach;?>

        </form>
        


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>
    <script type="text/javascript">
    
    $(document).ready(function() {
        var anchor = window.location.hash;
        $(".collapse").collapse('hide');
        $('#collapseTwo').collapse('show');
    });
    </script>
  </body>
</html>