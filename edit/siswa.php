<?php
require '../system/function.php';
$db = new crud();
$no = mysqli_real_escape_string($db->connection, $_GET['no']);

$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if (($res[0]['level'] == 'guru') or empty($no)  or ($no != substr($_SESSION['username'], 2))) {
  header('location: ' . base_url('dist/index.php'));
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Edit Data Siswa - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
  <?php
//cek nomor induk apakah ada ?
$ceking                = $db->where('t_siswa', array('nomor_induk' => $no));
if (empty(mysqli_num_rows($ceking))) {
  echo '<script type="text/javascript">
      swal({
      title: "Error!",
      text: "Nomor induk tidak ditemukan!",
      type: "error",
      confirmButtonText: "Oke"
    }, function() {
      window.location.assign(\'' . base_url('data/siswa.php') . '\');
    });</script>';
}


if (isset($_POST['simpan_siswa'])) {
  $nama               = mysqli_real_escape_string($db->connection, $_POST['nama_siswa']);
  $jk                 = mysqli_real_escape_string($db->connection, $_POST['jk']);
  $t_lahir            = mysqli_real_escape_string($db->connection, $_POST['t_lahir']);
  $ttl                = mysqli_real_escape_string($db->connection, $_POST['ttl']);
  $agama              =  mysqli_real_escape_string($db->connection, $_POST['agama']);
  $alamat             = mysqli_real_escape_string($db->connection, $_POST['alamat']);
  $telp               = mysqli_real_escape_string($db->connection,  '+62' . $_POST['telp']);
  // data sekolah
  $nama_sekolah       = mysqli_real_escape_string($db->connection, $_POST['nama_sekolah']);
  $alamat_sekolah     = mysqli_real_escape_string($db->connection, $_POST['alamat_sekolah']);
  $t_ijazah           = mysqli_real_escape_string($db->connection, $_POST['t_ijazah']);
  $no_ijazah          = mysqli_real_escape_string($db->connection, $_POST['no_ijazah']);
  $diterima           = mysqli_real_escape_string($db->connection, $_POST['diterima']);
  $tahun              = mysqli_real_escape_string($db->connection, $_POST['tahun']);
  // data orang tua
  $ayah               = mysqli_real_escape_string($db->connection, $_POST['ayah']);
  $ibu                = mysqli_real_escape_string($db->connection, $_POST['ibu']);
  $alamat_ortu        = mysqli_real_escape_string($db->connection, $_POST['alamat_ortu']);
  $telp_ortu          = mysqli_real_escape_string($db->connection,  '+62' . $_POST['telp_ortu']);
  $pekerjaan_ortu     = mysqli_real_escape_string($db->connection, $_POST['pekerjaan_ortu']);
  // data wali
  $wali               = mysqli_real_escape_string($db->connection, $_POST['wali']);
  $alamat_wali        = mysqli_real_escape_string($db->connection, $_POST['alamat_wali']);
  $telp_wali          = mysqli_real_escape_string($db->connection,  '+62' . $_POST['telp_wali']);
  $pekerjaan_wali     = mysqli_real_escape_string($db->connection, $_POST['pekerjaan_wali']);

  $cek                = $db->where('t_user', array('username' => $_SESSION['username']));
  $cek                = $cek->fetch_all(MYSQLI_ASSOC);

  //hanya sesuai user id atau admin yang dapat edit
  if (($_SESSION['username'] == $no) or ($cek[0]['level'] == 'admin')) {
    $simpan = $db->update('t_siswa', 
      array(
        'nama' => $nama, 
        'jk' => $jk, 
        'tempat_lahir' => $t_lahir, 
        'tanggal_lahir' => $ttl, 
        'agama' => $agama, 
        'alamat' => $alamat, 
        'telpon' => $telp,
        'sekolah_asal' => $nama_sekolah,
        'alamat_sekolah_asal' => $alamat_sekolah,
        'tahun_ijazah' => $t_ijazah,
        'nomor_ijazah' => $no_ijazah,
        'diterima_dikelas' => $diterima,
        'tahun_diterima' => $tahun,
        'nama_ayah' => $ayah,
        'nama_ibu' => $ibu,
        'alamat_ortu' => $alamat_ortu,
        'telp_ortu' => $telp_ortu,
        'pekerjaan_ortu' => $pekerjaan_ortu,
        'nama_wali' => $wali,
        'alamat_wali' => $alamat_wali,
        'telp_wali' => $telp_wali,
        'pekerjaan_wali' => $pekerjaan_wali
      ), array(
        'nomor_induk' => $no
      ));
    //end insert
    if (empty($simpan)) {
      echo '<script type="text/javascript">
        swal({
        title: "Great!",
        text: "Data berhasil disimpan!",
        type: "success",
        confirmButtonText: "Oke"
      }, function() {
        window.location.assign(\'' . base_url('data/siswa.php') . '\');
      });</script>';
    } else {
      echo '<script type="text/javascript">
        swal({
        title: "Error!",
        text: "Tampaknya ada kesalahan proses menyimpan",
        type: "error",
        confirmButtonText: "Oke"
      }, function() {
        window.location.assign(\'' . base_url('data/siswa.php') . '\');
      });</script>';
    }
  } else {
    echo '<script type="text/javascript">
      swal({
      title: "Error!",
      text: "Hak akses ditolak!",
      type: "error",
      confirmButtonText: "Oke"
    }, function() {
      window.location.assign(\'' . base_url('data/siswa.php') . '\');
    });</script>';
  }
}

?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
        <h1>Input Data Siswa</h1>
        <div class="alert alert-info" role="alert">
          <a href="<?php echo base_url('data/siswa.php');?>" class="btn btn-default"><span class="glyphicon glyphicon-menu-left"></span> Kembali</a>
        </div>

        <form method="post" class="form-horizontal">

        <?php
          $data       = $db->where('t_siswa', array('nomor_induk' => $no));
          foreach ($data as $value):
        ?>
          <div class="page-header">
            <h3>Data Siswa</h3>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Nomor Induk</label>
            <div class="col-sm-3">
              <input type="text" name="no_induk" class="form-control" placeholder="Nomor Induk" disabled="" value="<?php echo $value['nomor_induk'];?>">
            </div>
            <label class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-5">
              <input type="text" name="nama_siswa" class="form-control" placeholder="Nama Lengkap" value="<?php echo $value['nama'];?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Jenis Kelamin</label>
            <div class="col-sm-3">
              <select class="form-control" name="jk">
                <option>Jenis Kelamin</option>
                <?php
                if ($value['jk'] == 'L') {
                  echo '<option value="L" selected>Laki-laki</option>';
                } else {
                  echo '<option value="L">Laki-laki</option>';
                }

                if ($value['jk'] == 'P') {
                  echo '<option value="P" selected>Perempuan</option>';
                } else {
                  echo '<option value="P">Perempuan</option>';
                }

                ?>
              </select>
            </div>
            <label class="col-sm-2 control-label">Tempat Lahir</label>
            <div class="col-sm-4">
              <input type="text" name="t_lahir" class="form-control" placeholder="Tempat Lahir" value="<?php echo $value['tempat_lahir'];?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Tanggal Lahir</label>
            <div class="col-sm-3">
              <input type="text" name="ttl" class="form-control" placeholder="YYYY-MM-DD" value="<?php echo $value['tanggal_lahir'];?>">
            </div>
            <label class="col-sm-2 control-label">Agama</label>
            <div class="col-sm-3">
              <select class="form-control" name="agama">
                <option>Pilih Agama</option>
                <?php
                if ($value['agama'] == 'islam') {
                  echo '<option value="islam" selected>Islam</option>';
                } else {
                  echo '<option value="islam">Islam</option>';
                }

                if ($value['agama'] == 'katolik') {
                  echo '<option value="P" selected>Perempuan</option>';
                } else {
                  echo '<option value="katolik">Kriten Katolik</option>';
                }

                if ($value['agama'] == 'protestan') {
                  echo '<option value="protestan" selected>Kriten Protestan</option>';
                } else {
                  echo '<option value="protestan">Kriten Protestan</option>';
                }

                if ($value['agama'] == 'hindu') {
                  echo '<option value="hindu" selected>Hindu</option>';
                } else {
                  echo '<option value="hindu">Hindu</option>';
                }

                if ($value['agama'] == 'budha') {
                  echo '<option value="budha" selected>Budha</option>';
                } else {
                  echo '<option value="budha">Budha</option>';
                }

                ?>
                
                
                
                
                
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Alamat</label>
            <div class="col-sm-6">
              <textarea  name="alamat" class="form-control" rows="3" placeholder="Alamat Lengkap"><?php echo $value['alamat'];?></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">No Telp.</label>
            <div class="col-xs-1">
              <p class="form-control-static  control-label">+62</p>
            </div>
            <div class="col-sm-4">
              <input type="text" name="telp" class="form-control" placeholder="81234567890"  value="<?php echo substr($value['telpon'], 3);?>">
            </div>
          </div>


          <div class="page-header">
            <h3>Sekolah Asal</h3>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Nama Sekolah</label>
            <div class="col-sm-6">
              <input type="text" name="nama_sekolah" class="form-control" placeholder="Nama Sekolah Asal" value="<?php echo $value['sekolah_asal'];?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Alamat Sekolah</label>
            <div class="col-sm-10">
              <textarea class="form-control" name="alamat_sekolah" rows="3" placeholder="Alamat Lengkap"><?php echo $value['alamat_sekolah_asal'];?></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Tahun Ijazah</label>
            <div class="col-sm-3">
              <input type="text" name="t_ijazah" class="form-control" placeholder="<?php echo date('Y');?>" value="<?php echo $value['tahun_ijazah'];?>">
            </div>
            <label class="col-sm-2 control-label">Nomor Ijazah</label>
            <div class="col-sm-5">
              <input type="text" name="no_ijazah" class="form-control" placeholder="Nomor Ijazah" value="<?php echo $value['nomor_ijazah'];?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Diterima di Kelas</label>
            <div class="col-sm-3">
              <input type="text" name="diterima" class="form-control" placeholder="Diterima di kelas" value="<?php echo $value['diterima_dikelas'];?>">
            </div>
            <label class="col-sm-2 control-label">Tahun diterima</label>
            <div class="col-sm-3">
              <input type="text" name="tahun" class="form-control" placeholder="YYYY" value="<?php echo $value['tahun_diterima'];?>">
            </div>
          </div>

          <div class="page-header">
            <h3>Data Orang Tua</h3>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Nama Ayah</label>
            <div class="col-sm-5">
              <input type="text" name="ayah" class="form-control" placeholder="Nama Lengkap Ayah" value="<?php echo $value['nama_ayah'];?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Nama Ibu</label>
            <div class="col-sm-5">
              <input type="text" name="ibu" class="form-control" placeholder="Nama Lengkap Ibu" value="<?php echo $value['nama_ibu'];?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Alamat Orang Tua</label>
            <div class="col-sm-10">
              <textarea name="alamat_ortu" class="form-control" rows="3" placeholder="Alamat Lengkap Orang Tua"><?php echo $value['alamat_ortu'];?></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">No Telp.</label>
            <div class="col-xs-1">
              <p class="form-control-static  control-label">+62</p>
            </div>
            <div class="col-sm-4">
              <input type="text" name="telp_ortu" class="form-control" placeholder="81234567890" value="<?php echo substr($value['telp_ortu'], 3);?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Pekerjaan</label>
            <div class="col-sm-4">
              <input type="text" name="pekerjaan_ortu" class="form-control" placeholder="Pekerjaan Orang Tua" value="<?php echo $value['pekerjaan_ortu'];?>">
            </div>
          </div>


          <div class="page-header">
            <h3>Data Wali</h3>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Nama Wali</label>
            <div class="col-sm-5">
              <input type="text" name="wali" name="nisn" class="form-control" placeholder="Nama Lengkap Wali" value="<?php echo $value['nama_wali'];?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Alamat Wali</label>
            <div class="col-sm-10">
              <textarea class="form-control" name="alamat_wali" rows="3" placeholder="Alamat Lengkap Wali"><?php echo $value['alamat_wali'];?></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">No Telp.</label>
            <div class="col-xs-1">
              <p class="form-control-static  control-label">+62</p>
            </div>
            <div class="col-sm-4">
              <input type="text" name="telp_wali" class="form-control" placeholder="81234567890" value="<?php echo substr($value['telp_wali'], 3);?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Pekerjaan</label>
            <div class="col-sm-4">
              <input type="text" name="pekerjaan_wali" class="form-control" placeholder="Pekerjaan Wali" value="<?php echo $value['pekerjaan_wali'];?>">
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-4">
              <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
              <button type="submit" name="simpan_siswa" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
          </div>
          
          <?php endforeach;?>

        </form>
        


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#collapseOne').collapse('show');
    });
    </script>
  </body>
</html>