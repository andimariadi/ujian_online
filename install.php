<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Masuk Aplikasi - Computer Assisten Test</title>
    
    <?php require 'system/function.php';?>
    <?php require 'include/head.php';?>
  </head>
  <body class="login-body">
<?php

$db = new crud();
$cek_admin = $db->where('t_user', array('level' => 'admin'));
if (mysqli_num_rows($cek_admin) > 0) {
  header('location: ' . base_url('index.php'));
}

if (isset($_POST['register'])) {
  $user       = mysqli_real_escape_string($db->connection,$_POST['user']);
  $pass       = password_hash(mysqli_real_escape_string($db->connection,$_POST['pass']), PASSWORD_DEFAULT);
  $level      = 'admin';
  $simpan     = $db->insert('t_user', array(
    'username'  => $user,
    'password'  => $pass,
    'level'     => $level
    ));
  if (!$simpan) {
    echo '<script type="text/javascript">
      swal({
        title: "Register Admin Success!",
        text: "Please wait Anda akan dipindahkan kehalaman login.",
        timer: 3000,
        type: "success",
        showConfirmButton: false
      },
      function() {
        window.location.assign("' . base_url('index.php') . '");
      }
      );
    </script>';
  } else {
    echo '<script type="text/javascript">
      swal("Error!", "Kesalahan registrasi!", "error");
    </script>';
  }
  
}

?>
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4 white-body">
        <div class="panel panel-primary row login-header">
          <div class="panel-heading">
            <h1><span class="glyphicon glyphicon-info-sign"></span> Daftar Administrator</h1>
          </div>
        </div>
        <form method="post">
          <div class="form-group">
            <label><span class="glyphicon glyphicon-user"></span> Username</label>
            <input type="text" name="user" class="form-control" placeholder="Username" />
          </div>
          <div class="form-group">
            <label><span class="glyphicon glyphicon-lock"></span> Password</label>
            <input type="password" name="pass" class="form-control" placeholder="Password" />
          </div>
          <button name="register" type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-log-in"></span> Simpan</button>
          <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span> Reset</button>
        </form>
      </div>
    </div>
  </div>

    
  </body>
</html>