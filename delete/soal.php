<?php
require '../system/function.php';
$db = new crud();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Hapus</title>
	<?php include '../include/head.php';?>
</head>
<body>
	<?php

	$otoritasi 	= $db->where('t_user', array('username' => $_SESSION['username']));
	$res 		= $otoritasi->fetch_all(MYSQLI_ASSOC);
	if (($res[0]['level'] != 'guru') AND ($res[0]['level'] != 'admin')) {
		header('location: ' . base_url('dist/index.php'));
	} else {
		if (isset($_GET['no'])) {
			$no 		= mysqli_real_escape_string($db->connection, $_GET['no']);
			$cek 		= $db->where('t_soal', array('id_soal' => $no));
			
			if (mysqli_num_rows($cek) > 0) {
				$hapus = $db->delete('t_soal', array('id_soal' => $no));

				// hapus username on t_user
				$hapus = $db->delete('t_soal', array('id_soal' => $no));
				if (!$hapus) {
					echo '<script type="text/javascript">
						swal({
						  title: "Berhasil!",
						  text: "Data Berhasil Dihapus",
						  type: "success",
						  confirmButtonText: "Oke"
						},
						function() {
							window.location.assign(\'' . base_url('data/soal.php') . '\')
						});
					</script>';
				}
			} else {
				echo '<script type="text/javascript">
				swal({
				  title: "Error!",
				  text: "Nomor Soal Tidak Ditemukan",
				  type: "error",
				  confirmButtonText: "Oke"
				},
				function() {
					window.location.assign(\'' . base_url('data/soal.php') . '\')
				});
			</script>';
			}
			
		} else {
			header('location: ' . base_url('dist/index.php'));
		}
	}

	?>

</body>
</html>