<?php
require '../system/function.php';
$db = new crud();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Hapus</title>
	<?php include '../include/head.php';?>
</head>
<body>
	<?php

	//validasi delete
	$otoritasi 	= $db->where('t_user', array('username' => $_SESSION['username']));
	$res 		= $otoritasi->fetch_all(MYSQLI_ASSOC);
	if (($res[0]['level'] != 'guru') AND ($res[0]['level'] != 'admin')) {
		header('location: ' . base_url('dist/index.php'));
	} else {
		if (isset($_GET['id']) AND isset($_GET['nis'])) {
			$no 		= mysqli_real_escape_string($db->connection, $_GET['id']);
			$nis 		= mysqli_real_escape_string($db->connection, $_GET['nis']);
			$cek 		= $db->query("SELECT * FROM `t_cat_status` WHERE `nomor_induk`='{$nis}' AND `id_ujian`='{$no}'");
			
			if (mysqli_num_rows($cek) > 0) {
				$hapus = $db->query("DELETE FROM `t_cat_status` WHERE `nomor_induk`='{$nis}' AND `id_ujian`='{$no}'");
				$hapus = $db->query("DELETE FROM `t_cat` WHERE `nomor_induk_siswa`='{$nis}' AND `id_ujian`='{$no}'");

				if ($hapus) {
					echo '<script type="text/javascript">
						swal({
						  title: "Berhasil!",
						  text: "Data Berhasil Dihapus",
						  type: "success",
						  confirmButtonText: "Oke"
						},
						function() {
							window.location.assign(\'' . base_url('data/laporan_ujian.php') . '\')
						});
					</script>';
				}
			} else {
				echo '<script type="text/javascript">
				swal({
				  title: "Error!",
				  text: "Nomor Tidak Ditemukan",
				  type: "error",
				  confirmButtonText: "Oke"
				},
				function() {
					window.location.assign(\'' . base_url('data/laporan_ujian.php') . '\')
				});
			</script>';
			}
			
		} else {
			header('location: ' . base_url('data/laporan_ujian.php'));
		}
	}

	?>

</body>
</html>