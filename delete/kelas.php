<?php
require '../system/function.php';
$db = new crud();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Hapus</title>
	<?php include '../include/head.php';?>
</head>
<body>
	<?php

	//validasi delete
	$otoritasi 	= $db->where('t_user', array('username' => $_SESSION['username']));
	$res 		= $otoritasi->fetch_all(MYSQLI_ASSOC);
	if ($res[0]['level'] != 'admin') {
		header('location: ' . base_url('dist/index.php'));
	} else {
		if (isset($_GET['no'])) {
			$no 		= mysqli_real_escape_string($db->connection, $_GET['no']);
			$cek 		= $db->where('t_kelas', array('id_kelas' => $no));
			
			if (mysqli_num_rows($cek) > 0) {
				$hapus = $db->delete('t_kelas', array('id_kelas' => $no));
				if (!$hapus) {
					echo '<script type="text/javascript">
						swal({
						  title: "Berhasil!",
						  text: "Data Berhasil Dihapus",
						  type: "success",
						  confirmButtonText: "Oke"
						},
						function() {
							window.location.assign(\'' . base_url('data/kelas.php') . '\')
						});
					</script>';
				}
			} else {
				echo '<script type="text/javascript">
				swal({
				  title: "Error!",
				  text: "Nomor Induk Tidak Ditemukan",
				  type: "error",
				  confirmButtonText: "Oke"
				},
				function() {
					window.location.assign(\'' . base_url('data/kelas.php') . '\')
				});
			</script>';
			}
			
		} else {
			header('location: ' . base_url('dist/index.php'));
		}
	}

	?>

</body>
</html>