<?php
require '../system/function.php';
$db = new crud();

//hak akses
$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if (($res[0]['level'] != 'guru') AND ($res[0]['level'] != 'admin')) {
  header('location: ' . base_url('dist/index.php'));
}

if (isset($_GET['id'])) {
  $no = $_GET['id'];
} else {
  header('location: ' . base_url("data/ujian.php"));
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Data Ujian - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
        <h1>Laporan Ujian</h1>
        <div class="alert alert-info" role="alert">
          <p><a href="<?php echo base_url('data/ujian.php');?>" class="btn btn-default"><span class="glyphicon glyphicon-menu-left"></span> Kembali</a>
          <a href="#" class="btn btn-default" onclick="print('<?php echo base_url("print/ujian.php?id={$no}");?>')"><span class="glyphicon glyphicon-print" style="margin-right: 15px;"></span> Print</a>
          </p>
        </div>

      <div class="table-inner">
        <table class="table table-hover table-striped">
          <thead>
            <tr>
              <th>Nama Tes</th>
              <th>Mata Pelajaran</th>
              <th>Guru</th>
              <th>Jumlah Soal</th>
              <th>Jam / Tanggal</th>
              <th>Waktu</th>
            </tr>
          </thead>
          <tbody>
          <?php
            $list = $db->query('SELECT * FROM `t_ujian` LEFT JOIN `t_guru` ON `t_ujian`.`nip_guru`=`t_guru`.`nip` LEFT JOIN `t_mapel` ON `t_ujian`.`kode_mapel`=`t_mapel`.`kode_mapel` WHERE `t_ujian`.`id_ujian`=\'' . $no . '\'')->fetch_all(MYSQLI_ASSOC);
          foreach ($list as $element) {
            echo "<tr>";
            echo "
              <td>{$element['nama_ujian']}</td>
              <td>{$element['kode_mapel']} / {$element['nama_mapel']}</td>
              <td>{$element['nip']} / {$element['nama']}</td>
              <td>{$element['jumlah_soal']}</td>
              <td>{$element['jam_ujian']} / {$element['tanggal']}</td>
              <td>{$element['waktu_ujian']} menit</td>
              ";
           
              echo "
            </tr>";
          }


          ?>
          </tbody>
        </table>
      </div>

      <h2>Daftar Peserta Ujian</h2>

      <div class="table-inner">
        <table class="table table-hover table-striped">
          <thead>
            <tr>
              <th>NIS</th>
              <th>Nama Peserta</th>
              <th>Benar</th>
              <th>Salah</th>
              <th>Score</th>
              <th></th>
            </tr>
          </thead>
          <tbody>

      <?php

        $status = $db->query("SELECT * FROM `t_cat_status`
          LEFT JOIN `t_siswa` ON `t_cat_status`.`nomor_induk` = `t_siswa`.`nomor_induk`
         WHERE `status`='T' AND `t_cat_status`.`id_ujian`='{$no}'")->fetch_all(MYSQLI_ASSOC);
        foreach ($status as $value) {
          //menghitung benar salahnya
          $hitung_nilai = $db->query("SELECT `t_cat`.`list_jawaban`,`t_soal`.`jawaban` FROM `t_cat` LEFT JOIN `t_soal` ON `t_cat`.`list_soal` = `t_soal`.`id_soal` WHERE `t_cat`.`id_ujian`='{$no}' AND `t_cat`.`nomor_induk_siswa`='{$value['nomor_induk']}'")->fetch_all(MYSQLI_ASSOC);
          $benar = 0;
          foreach ($hitung_nilai as $element) {
            if ($element['list_jawaban'] == $element['jawaban']) {
              $benar++;
            }
          }
          echo '<tr>';
          echo '<td>' . $value['nomor_induk'] . '</td>';
          echo '<td>' . $value['nama'] . '</td>';
          echo '<td>' . $benar . '</td>';
          echo '<td>' . number_format($list[0]['jumlah_soal']-$benar) . '</td>';
          echo '<td><strong>' . number_format(100/$list[0]['jumlah_soal']*$benar, 2) . '%</strong></td>';
          echo '<td><a href="'. base_url("dist/final_tes.php?id={$no}&nis={$value['nomor_induk']}") .'" class="btn btn-success btn-xs">
                <i class="glyphicon glyphicon-search"></i> Laporan
              </a>
              <a href="'. base_url("delete/laporan_ujian.php?id={$no}&nis={$value['nomor_induk']}") .'" class="btn btn-danger btn-xs">
                <i class="glyphicon glyphicon-trash"></i> Batalkan ujian
              </a>
              </td>';
          echo '</tr>';
          
        }
      ?>
          </tbody>
        </table>
      </div>


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>
    <script type="text/javascript">
      function hapus($value) {
        swal({
          title: "Are you sure?",
          text: "Semua yang berhubungan dengan data ini akan terhapus otomatis!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        function(isConfirm){
          if (isConfirm) {
            window.location.assign('<?php echo base_url('delete/ujian.php?no=');?>' + $value);
          }
        });
      }
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.collapse').collapse('hide');
        $('#collapseFive').collapse('show');
    });
    </script>
  </body>
</html>