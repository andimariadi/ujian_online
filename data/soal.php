<?php
require '../system/function.php';
$db = new crud();

//hak akses
$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if (($res[0]['level'] != 'guru') AND ($res[0]['level'] != 'admin')) {
  header('location: ' . base_url('dist/index.php'));
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bank Soal - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
        <h1>Data Soal</h1>
        <div class="alert alert-info" role="alert">
          <a href="<?php echo base_url('entry/soal.php');?>" class="btn btn-primary">Tambah</a>
          <a href="#" class="btn btn-default"  data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-print" style="margin-right: 15px;"></span> Print</a>
          <a href="#" class="btn btn-default" onclick="print('<?php echo base_url("print/soal.php");?>')"><span class="glyphicon glyphicon-print" style="margin-right: 15px;"></span> Print All</a>
        </div>

      <div class="table-inner">
        <table class="table table-hover table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Soal</th>
              <th>Mata Pelajaran</th>
              <th>NIP / Nama Guru</th>
              <th>Kelas</th>
            </tr>
          </thead>
          <tbody>
          <?php
          if ($res[0]['level'] == 'guru') {
            $list = $db->query('SELECT `t_soal`.`id_soal`,`t_soal`.`soal`, `t_soal`.`nip_guru`,`t_mapel`.`kode_mapel`,`t_mapel`.`nama_mapel`, `t_guru`.`nama`,`t_kelas`.`kelas` FROM `t_soal` 
              LEFT JOIN `t_guru` ON `t_soal`.`nip_guru` = `t_guru`.`nip` 
              LEFT JOIN `t_mapel` ON `t_soal`.`kode_mapel` = `t_mapel`.`kode_mapel`
              LEFT JOIN `t_kelas` ON `t_soal`.`id_kelas` = `t_kelas`.`id_kelas` WHERE `t_soal`.`nip_guru`=\'' . substr($res[0]['username'], 2) . '\'')->fetch_all(MYSQLI_ASSOC);
          } elseif ($res[0]['level'] == 'admin') {
            $list = $db->query('SELECT `t_soal`.`id_soal`,`t_soal`.`soal`, `t_soal`.`nip_guru`, `t_mapel`.`kode_mapel`,`t_mapel`.`nama_mapel`, `t_guru`.`nama`,`t_kelas`.`kelas` FROM `t_soal` LEFT JOIN `t_guru` ON `t_soal`.`nip_guru` = `t_guru`.`nip` LEFT JOIN `t_mapel`ON `t_soal`.`kode_mapel` = `t_mapel`.`kode_mapel` LEFT JOIN `t_kelas` ON `t_soal`.`id_kelas` = `t_kelas`.`id_kelas`')->fetch_all(MYSQLI_ASSOC);
          }
          foreach ($list as $element) {
            echo "<tr>
              <td>";
              echo '
              <a href="'.base_url('edit/soal.php?no=' . $element['id_soal']).'" class="btn btn-info btn-xs">
                <i class="glyphicon glyphicon-pencil"></i>
              </a>
              <a href="#" class="btn btn-danger btn-xs" onclick="hapus(\'' . $element['id_soal'] . '\')">
                <i class="glyphicon glyphicon-remove"></i>
              </a>';
              echo "</td>
              <td>{$element['soal']}</td>
              <td>{$element['kode_mapel']} / {$element['nama_mapel']}</td>
              <td>{$element['nip_guru']} / {$element['nama']}</td>
              <td>{$element['kelas']}</td>
            </tr>";
          }


          ?>
          </tbody>
        </table>
      </div>


      </div>
    </div>
  </div>


  <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Print Soal</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

    <?php require '../include/footer.php';?>
    <script type="text/javascript">
      function hapus($value) {
        swal({
          title: "Are you sure?",
          text: "Semua yang berhubungan dengan data ini akan terhapus otomatis!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        function(isConfirm){
          if (isConfirm) {
            window.location.assign('<?php echo base_url('delete/soal.php?no=');?>' + $value);
          }
        });
      }
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.collapse').collapse('hide');
        $('#collapseFour').collapse('show');
    });
    </script>
  </body>
</html>