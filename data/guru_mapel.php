<?php
require '../system/function.php';
$db = new crud();

//hak akses
$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if ($res[0]['level'] != 'admin') {
  header('location: ' . base_url('dist/index.php'));
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mata Pelajaran Yang Diampu- Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>

      <?php


          $cek_database = $db->view('t_guru');


      ?>
      <div class="col-md-9 white-body">
        <h1>Mata Pelajaran Yang Diampu</h1>
        
          <?php
          
          if (count($cek_database) == 0) {
            echo "<div class=\"alert alert-danger\" role=\"alert\"><p>Data guru tidak ditemukan. Masukkan terlebih dahulu! <a href=\"".base_url('entry/guru.php')."\">Entry Guru <i class=\"glyphicon glyphicon-new-window\"></i></a></div>";
          }

          ?>
        <div class="alert alert-info" role="alert">
        
          <a href="<?php echo base_url('entry/guru_mapel.php');?>" class="btn btn-primary" <?php
          
          if (count($cek_database) == 0) {
            echo "disabled";
          }

          ?>>Tambah</a>
          <p>Tambahkan mata pelajaran yang diampu oleh guru. Mata pelajaran harus sesuai dengan mata pelajaran yang diampu. Hal ini diperlukan untuk guru membuat soal yang sesuai dengan mata pelajaran yang diampunya.</p>
        </div>

      <div class="table-inner">
        <table class="table table-hover table-striped table-bordered">
          <thead>
            <tr>
              <th></th>
              <th>NIP</th>
              <th>Nama</th>
              <th>Mata Pelajaran</th>
            </tr>
          </thead>
          <tbody>
          <?php
          
          $list = $db->join('t_guru_mapel', array('t_guru'), array('t_guru_mapel.nip_guru' => 't_guru.nip'));
          foreach ($list as $element) {
            $ex = explode(',', $element['no_mapel']);
            echo "<tr>
              <td>";
              echo '
              <a href="'.base_url('edit/guru_mapel.php?no=' . $element['no']).'" class="btn btn-info btn-xs">
                <i class="glyphicon glyphicon-pencil"></i>
              </a>
              <a href="#" class="btn btn-danger btn-xs" onclick="hapus(\'' . $element['no'] . '\')">
                <i class="glyphicon glyphicon-remove"></i>
              </a>';
              echo "</td>
              <td>{$element['nip']}</td>
              <td>{$element['nama']}</td>";
              echo "<td>";
              for ($i=0; $i < count($ex); $i++) { 
                $mapel = $db->where('t_mapel', array('kode_mapel' => $ex[$i]))->fetch_all(MYSQLI_ASSOC);
                foreach ($mapel as $key) {
                  echo "<span class=\"label label-primary\">{$key['nama_mapel']}</span> ";
                }
              }
              echo "</td>
            </tr>";
          }


          ?>
          </tbody>
        </table>
      </div>


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>

    <script type="text/javascript">
      function hapus($value) {
        swal({
          title: "Are you sure?",
          text: "Semua yang berhubungan dengan data ini akan terhapus otomatis!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        function(isConfirm){
          if (isConfirm) {
            window.location.assign('<?php echo base_url('delete/guru_mapel.php?no=');?>' + $value);
          }
        });
      }
    </script>
    <script type="text/javascript">
    
    $(document).ready(function() {
        var anchor = window.location.hash;
        $(".collapse").collapse('hide');
        $('#collapseTwo').collapse('show');
    });
    </script>
  </body>
</html>