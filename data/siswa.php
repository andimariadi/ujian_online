<?php
require '../system/function.php';
$db = new crud();

//hak akses
$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if ($res[0]['level'] != 'admin') {
  header('location: ' . base_url('dist/index.php'));
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Data Siswa - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
        <h1>Data Siswa</h1>
        <div class="alert alert-info" role="alert">
          <a href="<?php echo base_url('entry/siswa.php');?>" class="btn btn-primary">Tambah</a>
          <a href="#" class="btn btn-default" onclick="print('<?php echo base_url("print/siswa.php");?>')"><span class="glyphicon glyphicon-print" style="margin-right: 15px;"></span> Print</a>
          <p>Untuk masuk sistem gunakan kode M-Nomor Induk dengan password Nomor Induk, misalnya username : M-123456, kata sandi : 123456.</p>
        </div>

      <div class="table-inner">
        <table class="table table-hover table-striped table-bordered">
          <thead>
            <tr>
              <th></th>
              <th>No. Induk</th>
              <th>Nama Lengkap</th>
              <th>Jenis Kelamin</th>
              <th>TTL</th>
              <th>Agama</th>
              <th>Alamat</th>
              <th>Telp.</th>

              <th>Sekolah Asal</th>
              <th>Alamat Sekolah Asal</th>
              <th>Tahun Ijazah</th>
              <th>Nomor Ijazah</th>
              <th>Diterima Di</th>
              <th>Tanggal</th>
              <th>Ayah</th>
              <th>Ibu</th>
              <th>Alamat Ortu</th>
              <th>Telp. Ortu</th>
              <th>Pekerjaan</th>
              <th>Wali</th>
              <th>Alamat Wali</th>
              <th>Telp. Wali</th>
              <th>Pekerjaan</th>
            </tr>
          </thead>
          <tbody>
          <?php
          
          $list_siswa = $db->view('t_siswa');
          foreach ($list_siswa as $element) {
            echo "<tr>
              <td>";
              echo '
              <a href="'.base_url('edit/siswa.php?no=' . $element['nomor_induk']).'" class="btn btn-info btn-xs">
                <i class="glyphicon glyphicon-pencil"></i>
              </a>
              <a href="#" class="btn btn-danger btn-xs" onclick="hapus_siswa(\'' . $element['nomor_induk'] . '\')">
                <i class="glyphicon glyphicon-remove"></i>
              </a>
              <a href="#" class="btn btn-default btn-xs" onclick="print(\'' . base_url("print/detail_siswa.php?id={$element['nomor_induk']}") . '\')">
                <i class="glyphicon glyphicon-print"></i>
              </a>
              ';
              echo "</td>
              <td>{$element['nomor_induk']}</td>
              <td>{$element['nama']}</td>
              <td>{$element['jk']}</td>
              <td>{$element['tempat_lahir']}, {$element['tanggal_lahir']}</td>
              <td>{$element['agama']}</td>
              <td>{$element['alamat']}</td>
              <td>{$element['telpon']}</td>
              <td>{$element['sekolah_asal']}</td>
              <td>{$element['alamat_sekolah_asal']}</td>
              <td>{$element['tahun_ijazah']}</td>
              <td>{$element['nomor_ijazah']}</td>
              <td>{$element['diterima_dikelas']}</td>
              <td>{$element['tahun_diterima']}</td>
              <td>{$element['nama_ayah']}</td>
              <td>{$element['nama_ibu']}</td>
              <td>{$element['alamat_ortu']}</td>
              <td>{$element['telp_ortu']}</td>
              <td>{$element['pekerjaan_ortu']}</td>
              <td>{$element['nama_wali']}</td>
              <td>{$element['alamat_wali']}</td>
              <td>{$element['telp_wali']}</td>
              <td>{$element['pekerjaan_wali']}</td>
            </tr>";
          }


          ?>
          </tbody>
        </table>
      </div>


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>

    <script type="text/javascript">
      function hapus_siswa($value) {
        swal({
          title: "Are you sure?",
          text: "Semua yang berhubungan dengan data ini akan terhapus otomatis!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        function(isConfirm){
          if (isConfirm) {
            window.location.assign('<?php echo base_url('delete/siswa.php?no=');?>' + $value);
          }
        });
      }
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#collapseOne').collapse('show');
    });
    </script>
  </body>
</html>