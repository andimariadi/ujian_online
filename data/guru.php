<?php
require '../system/function.php';
$db = new crud();

//hak akses
$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if ($res[0]['level'] != 'admin') {
  header('location: ' . base_url('dist/index.php'));
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Data Guru - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
        <h1>Data Guru</h1>
        <div class="alert alert-info" role="alert">
          <a href="<?php echo base_url('entry/guru.php');?>" class="btn btn-primary">Tambah</a>
          <p>Untuk masuk sistem gunakan kode G-Nomor Induk Pegawai dengan password Nomor Induk Pegawai, misalnya username : G-123456, kata sandi : 123456.</p>
        </div>

      <div class="table-inner">
        <table class="table table-hover table-striped table-bordered">
          <thead>
            <tr>
              <th></th>
              <th>NIP</th>
              <th>Nama Lengkap</th>
              <th>Alamat</th>
              <th>Telp.</th>
              <th>Username</th>
            </tr>
          </thead>
          <tbody>
          <?php
          
          $list = $db->view('t_guru');
          foreach ($list as $element) {
            echo "<tr>
              <td>";
              echo '
              <a href="'.base_url('edit/guru.php?no=' . $element['nip']).'" class="btn btn-info btn-xs">
                <i class="glyphicon glyphicon-pencil"></i>
              </a>
              <a href="#" class="btn btn-danger btn-xs" onclick="hapus(\'' . $element['nip'] . '\')">
                <i class="glyphicon glyphicon-remove"></i>
              </a>';
              echo "</td>
              <td>{$element['nip']}</td>
              <td>{$element['nama']}</td>
              <td>{$element['alamat']}</td>
              <td>{$element['telp']}</td>
              <td>G-{$element['nip']}</td>
            </tr>";
          }


          ?>
          </tbody>
        </table>
      </div>


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>

    <script type="text/javascript">
      function hapus($value) {
        swal({
          title: "Are you sure?",
          text: "Semua yang berhubungan dengan data ini akan terhapus otomatis!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        function(isConfirm){
          if (isConfirm) {
            window.location.assign('<?php echo base_url('delete/guru.php?no=');?>' + $value);
          }
        });
      }
    </script>
    <script type="text/javascript">
    
    $(document).ready(function() {
        var anchor = window.location.hash;
        $(".collapse").collapse('hide');
        $('#collapseTwo').collapse('show');
    });
    </script>
  </body>
</html>