-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 02, 2017 at 02:20 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ujian`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_cat`
--

CREATE TABLE `t_cat` (
  `id_cat` int(10) NOT NULL,
  `id_ujian` int(10) NOT NULL,
  `nomor_induk_siswa` int(11) NOT NULL,
  `list_soal` int(5) NOT NULL,
  `list_jawaban` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_cat_status`
--

CREATE TABLE `t_cat_status` (
  `id_status` int(10) NOT NULL,
  `nomor_induk` int(10) NOT NULL,
  `id_ujian` int(10) NOT NULL,
  `status` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_guru`
--

CREATE TABLE `t_guru` (
  `nip` int(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_guru_mapel`
--

CREATE TABLE `t_guru_mapel` (
  `no` int(11) NOT NULL,
  `nip_guru` int(20) NOT NULL,
  `no_mapel` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_kelas`
--

CREATE TABLE `t_kelas` (
  `id_kelas` int(5) NOT NULL,
  `kelas` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `t_mapel`
--

CREATE TABLE `t_mapel` (
  `kode_mapel` varchar(5) NOT NULL,
  `nama_mapel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_siswa`
--

CREATE TABLE `t_siswa` (
  `nomor_induk` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jk` varchar(1) NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `agama` varchar(10) NOT NULL,
  `alamat` text NOT NULL,
  `telpon` varchar(15) NOT NULL,
  `sekolah_asal` varchar(50) NOT NULL,
  `alamat_sekolah_asal` text NOT NULL,
  `tahun_ijazah` varchar(4) NOT NULL,
  `nomor_ijazah` varchar(25) NOT NULL,
  `diterima_dikelas` varchar(10) NOT NULL,
  `tahun_diterima` varchar(4) NOT NULL,
  `nama_ayah` varchar(50) NOT NULL,
  `nama_ibu` varchar(50) NOT NULL,
  `alamat_ortu` text NOT NULL,
  `telp_ortu` varchar(15) NOT NULL,
  `pekerjaan_ortu` varchar(50) NOT NULL,
  `nama_wali` varchar(50) NOT NULL,
  `alamat_wali` text NOT NULL,
  `telp_wali` varchar(15) NOT NULL,
  `pekerjaan_wali` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_soal`
--

CREATE TABLE `t_soal` (
  `id_soal` int(10) NOT NULL,
  `nip_guru` int(20) NOT NULL,
  `kode_mapel` varchar(5) NOT NULL,
  `id_kelas` int(5) NOT NULL,
  `soal` text NOT NULL,
  `opsi_a` text NOT NULL,
  `opsi_b` text NOT NULL,
  `opsi_c` text NOT NULL,
  `opsi_d` text NOT NULL,
  `opsi_e` text NOT NULL,
  `jawaban` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_ujian`
--

CREATE TABLE `t_ujian` (
  `id_ujian` int(100) NOT NULL,
  `id_kelas` int(5) NOT NULL,
  `nama_ujian` varchar(100) NOT NULL,
  `nip_guru` int(20) NOT NULL,
  `kode_mapel` varchar(5) NOT NULL,
  `jumlah_soal` int(5) NOT NULL,
  `jam_ujian` time NOT NULL,
  `tanggal` date NOT NULL,
  `waktu_ujian` int(10) NOT NULL,
  `type` varchar(20) NOT NULL,
  `token` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `no_user` int(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_cat`
--
ALTER TABLE `t_cat`
  ADD PRIMARY KEY (`id_cat`),
  ADD KEY `id_ujian` (`id_ujian`),
  ADD KEY `nomor_induk_siswa` (`nomor_induk_siswa`),
  ADD KEY `list_soal` (`list_soal`);

--
-- Indexes for table `t_cat_status`
--
ALTER TABLE `t_cat_status`
  ADD PRIMARY KEY (`id_status`),
  ADD KEY `nomor_induk` (`nomor_induk`),
  ADD KEY `id_ujian` (`id_ujian`);

--
-- Indexes for table `t_guru`
--
ALTER TABLE `t_guru`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `t_guru_mapel`
--
ALTER TABLE `t_guru_mapel`
  ADD PRIMARY KEY (`no`),
  ADD KEY `nip` (`nip_guru`),
  ADD KEY `nip_2` (`nip_guru`),
  ADD KEY `no_mapel` (`no_mapel`);

--
-- Indexes for table `t_kelas`
--
ALTER TABLE `t_kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `t_mapel`
--
ALTER TABLE `t_mapel`
  ADD PRIMARY KEY (`kode_mapel`);

--
-- Indexes for table `t_siswa`
--
ALTER TABLE `t_siswa`
  ADD PRIMARY KEY (`nomor_induk`);

--
-- Indexes for table `t_soal`
--
ALTER TABLE `t_soal`
  ADD PRIMARY KEY (`id_soal`),
  ADD KEY `nip_guru` (`nip_guru`),
  ADD KEY `kode_mapel` (`kode_mapel`),
  ADD KEY `id_kelas` (`id_kelas`);

--
-- Indexes for table `t_ujian`
--
ALTER TABLE `t_ujian`
  ADD PRIMARY KEY (`id_ujian`),
  ADD KEY `nip_guru` (`nip_guru`),
  ADD KEY `kode_mapel` (`kode_mapel`),
  ADD KEY `id_kelas` (`id_kelas`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`no_user`),
  ADD KEY `parent_id` (`level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_cat`
--
ALTER TABLE `t_cat`
  MODIFY `id_cat` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_cat_status`
--
ALTER TABLE `t_cat_status`
  MODIFY `id_status` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_guru_mapel`
--
ALTER TABLE `t_guru_mapel`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_kelas`
--
ALTER TABLE `t_kelas`
  MODIFY `id_kelas` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_soal`
--
ALTER TABLE `t_soal`
  MODIFY `id_soal` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_ujian`
--
ALTER TABLE `t_ujian`
  MODIFY `id_ujian` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `no_user` int(10) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_cat`
--
ALTER TABLE `t_cat`
  ADD CONSTRAINT `t_cat_ibfk_1` FOREIGN KEY (`id_ujian`) REFERENCES `t_ujian` (`id_ujian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_cat_ibfk_2` FOREIGN KEY (`nomor_induk_siswa`) REFERENCES `t_siswa` (`nomor_induk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_cat_ibfk_3` FOREIGN KEY (`list_soal`) REFERENCES `t_soal` (`id_soal`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_cat_status`
--
ALTER TABLE `t_cat_status`
  ADD CONSTRAINT `t_cat_status_ibfk_1` FOREIGN KEY (`nomor_induk`) REFERENCES `t_siswa` (`nomor_induk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_cat_status_ibfk_2` FOREIGN KEY (`id_ujian`) REFERENCES `t_ujian` (`id_ujian`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_guru_mapel`
--
ALTER TABLE `t_guru_mapel`
  ADD CONSTRAINT `t_guru_mapel_ibfk_3` FOREIGN KEY (`nip_guru`) REFERENCES `t_guru` (`NIP`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_guru_mapel_ibfk_4` FOREIGN KEY (`no_mapel`) REFERENCES `t_mapel` (`kode_mapel`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_soal`
--
ALTER TABLE `t_soal`
  ADD CONSTRAINT `t_soal_ibfk_1` FOREIGN KEY (`nip_guru`) REFERENCES `t_guru` (`NIP`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_soal_ibfk_2` FOREIGN KEY (`kode_mapel`) REFERENCES `t_mapel` (`kode_mapel`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_soal_ibfk_3` FOREIGN KEY (`id_kelas`) REFERENCES `t_kelas` (`id_kelas`);

--
-- Constraints for table `t_ujian`
--
ALTER TABLE `t_ujian`
  ADD CONSTRAINT `t_ujian_ibfk_1` FOREIGN KEY (`nip_guru`) REFERENCES `t_guru` (`NIP`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_ujian_ibfk_2` FOREIGN KEY (`kode_mapel`) REFERENCES `t_mapel` (`kode_mapel`),
  ADD CONSTRAINT `t_ujian_ibfk_3` FOREIGN KEY (`id_kelas`) REFERENCES `t_kelas` (`id_kelas`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
