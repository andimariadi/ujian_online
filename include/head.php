<!-- Bootstrap -->
<link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/styles.css');?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/sweetalert.css');?>" rel="stylesheet">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/sweetalert.min.js');?>"></script>
<script src="<?php echo base_url('assets/tinymce/tinymce.min.js');?>"></script>