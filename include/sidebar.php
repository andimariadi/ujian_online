<?php
$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
?>

<nav class="navbar navbar-clean">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title"> 
        <a href="<?php echo base_url('dist/index.php');?>">
          <span class="glyphicon glyphicon-dashboard"></span> Dashboard
        </a>
      </h4>
    </div>
  </div>

  <?php if ($res[0]['level'] == 'admin') {?>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title"> 
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Data Siswa <span class="glyphicon glyphicon-chevron-down kanan"></span>
        </a>
      </h4>
    </div>
    
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <ul class="list-group">
        <li class="list-group-item"><span class="glyphicon glyphicon-list-alt"></span> <a href="<?php echo base_url('data/siswa.php');?>">Data Siswa</a></li>
        <li class="list-group-item"><span class=" glyphicon glyphicon-plus"></span> <a href="<?php echo base_url('entry/siswa.php');?>">Tambah Siswa</a>
        <li class="list-group-item"><span class="glyphicon glyphicon-search"></span> <a href="#" onclick="print('<?php echo base_url("print/siswa.php");?>')">Laporan Data</a></li></li>
      </ul>
    </div>
  </div>


  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Data Guru <span class="glyphicon glyphicon-chevron-down kanan"></span>
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <ul class="list-group">
        <li class="list-group-item"><span class="glyphicon glyphicon-list-alt"></span> <a href="<?php echo base_url('data/guru.php');?>">Data Guru</a></li>
        <li class="list-group-item"><span class=" glyphicon glyphicon-th-large"></span> <a href="<?php echo base_url('data/guru_mapel.php');?>">Guru Mata Pelajaran</a></li>
      </ul>
    </div>
  </div>


  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Mata Pelajaran <span class="glyphicon glyphicon-chevron-down kanan"></span>
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <ul class="list-group">
        <li class="list-group-item"><span class="glyphicon glyphicon-list-alt"></span> <a href="<?php echo base_url('data/mapel.php');?>">Data Mata Pelajaran</a></li>
        <li class="list-group-item"><span class="glyphicon glyphicon-plus"></span> <a href="<?php echo base_url('entry/mapel.php');?>">Tambah Mata Pelajaran</a></li>
      </ul>
    </div>
  </div>
  <?php }?>
  
  <?php if ($res[0]['level'] != 'murid') {?>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          Soal  <span class="glyphicon glyphicon-chevron-down kanan"></span>
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <ul class="list-group">
        <li class="list-group-item"><span class="glyphicon glyphicon-list-alt"></span> <a href="<?php echo base_url('data/soal.php');?>">Bank Soal</a></li>
        <li class="list-group-item"><span class="glyphicon glyphicon-plus"></span> <a href="#">Tambah Soal</a></li>
      </ul>
    </div>
  </div>
  <?php } ?>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
          Ujian  <span class="glyphicon glyphicon-chevron-down kanan"></span>
        </a>
      </h4>
    </div>
    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <ul class="list-group">
      <?php if ($res[0]['level'] != 'murid') {?>
          <li class="list-group-item"><span class="glyphicon glyphicon-list-alt"></span> <a href="<?php echo base_url('data/ujian.php');?>">Data Ujian</a></li>
      <?php 
      }

      if ($res[0]['level'] == 'murid') {?>
          <li class="list-group-item"><span class="glyphicon glyphicon-list-alt"></span> <a href="<?php echo base_url('dist/ujian.php');?>">Lihat Ujian</a></li>
          <li class="list-group-item"><span class="glyphicon glyphicon-search"></span> <a href="<?php echo base_url('dist/laporan_ujian.php');?>">Laporan Ujian</a></li>
      <?php  }?>
        </ul>
    </div>
  </div>


  <?php if ($res[0]['level'] == 'admin') {?>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseKelas" aria-expanded="false" aria-controls="collapseKelas">
          Kelas  <span class="glyphicon glyphicon-chevron-down kanan"></span>
        </a>
      </h4>
    </div>
    <div id="collapseKelas" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <ul class="list-group">
          <li class="list-group-item"><span class="glyphicon glyphicon-list-alt"></span> <a href="<?php echo base_url('data/kelas.php');?>">Data Kelas</a></li>
        </ul>
    </div>
  </div>
  <?php  }?>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
          <span class="glyphicon glyphicon-user"></span> Username: <?php echo @$_SESSION['username'];?> <span class="glyphicon glyphicon-chevron-down kanan"></span>
        </a>
      </h4>
    </div>
    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <ul class="list-group">
        <?php if ($res[0]['level'] == 'murid') {?>
          <li class="list-group-item"><span class="glyphicon glyphicon-user"></span> <a href="<?php echo base_url('edit/siswa.php?no=' . substr($_SESSION['username'], 2));?>">Edit Profile</a></li>
        <?php } ?>

        <?php if ($res[0]['level'] == 'guru') {?>
          <li class="list-group-item"><span class="glyphicon glyphicon-user"></span> <a href="<?php echo base_url('edit/guru.php?no=' . substr($_SESSION['username'], 2));?>">Edit Profile</a></li>
        <?php } ?>
          <li class="list-group-item"><span class="glyphicon glyphicon-lock"></span> <a href="<?php echo base_url('entry/password.php');?>">Ubah Password</a></li>
          <li class="list-group-item"><span class="glyphicon glyphicon-log-out"></span> <a href="<?php echo base_url('logout.php');?>">Logout!</a></li>
        </ul>
    </div>
  </div>
</div>
</div><!-- /.navbar-collapse -->
    </nav>