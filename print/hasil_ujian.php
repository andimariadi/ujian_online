<?php
require '../system/function.php';
$db = new crud();

//hak akses
if ((empty($_GET['nis']) or empty($_GET['id'])) OR empty($_SESSION['username'])) {
  header('location: ' . base_url('dist/'));
}

$nis = $_GET['nis'];
$no = $_GET['id'];

?>
<!DOCTYPE html>
<html>
<head>
  <title>Laporan Hasil Ujian</title>
  <link href='<?php echo base_url('assets/css/print_styles.css');?>' rel='stylesheet' media='' type='text/css'/>
</head>
<body onload="javascript:window.print()">

<h3>Laporan Hasil Ujian</h3>
<hr style="border: solid 1px #000"><br>


<h4>Detil Peserta</h4>

<table class="table-bordered" style="margin-bottom: 0px">

<?php
  $peserta = $db->where('t_siswa', array('nomor_induk' => $nis))->fetch_all(MYSQLI_ASSOC);
?>
  <tr><td width="30%">Nomor Induk Siswa</td><td><b><?php echo $peserta[0]['nomor_induk'];?></b></td></tr>
  <tr><td>Nama Lengkap</td><td width="70%"><b><?php echo $peserta[0]['nama'];?></b></td></tr>
</table>
<br><br>


<?php

//menghitung benar salahnya
$hitung_nilai = $db->query("SELECT `t_cat`.`list_jawaban`,`t_soal`.`jawaban` FROM `t_cat` LEFT JOIN `t_soal` ON `t_cat`.`list_soal` = `t_soal`.`id_soal` WHERE `t_cat`.`id_ujian`='{$no}' AND `t_cat`.`nomor_induk_siswa`='{$nis}'")->fetch_all(MYSQLI_ASSOC);
$benar = 0;

foreach ($hitung_nilai as $element) {
  if ($element['list_jawaban'] == $element['jawaban']) {
    $benar++;
  }
}
          
//echo $nilai;
//query data ujian
$query = $db->query("SELECT `nama_ujian`,`nip_guru`,`t_guru`.`nama`,`t_mapel`.`kode_mapel`,`t_mapel`.`nama_mapel`,`jumlah_soal`,`jam_ujian`,`tanggal`,`waktu_ujian` FROM `t_ujian` LEFT JOIN `t_guru` ON `t_ujian`.`nip_guru` = `t_guru`.`nip`LEFT JOIN `t_mapel` ON `t_ujian`.`kode_mapel` = `t_mapel`.`kode_mapel` WHERE `id_ujian` = '{$no}'");
$query = $query->fetch_all(MYSQLI_ASSOC);
?>

<h4>Hasil Ujian</h4>
<table class="table-bordered">
  <tr><td width="30%">Nama Ujian</td><td><b><?php echo $query[0]['nama_ujian'];?></b></td><td style="text-align: center;"><strong>Score</strong></td></tr>
  <tr><td width="30%">Mata Pelajaran</td><td><b><?php echo $query[0]['kode_mapel'];?> / <?php echo $query[0]['nama_mapel'];?></b></td>
    <td rowspan="7" style="vertical-align: middle; font-size: 3em;text-align: center;">
      <strong><?php echo number_format(100/$query[0]['jumlah_soal']*$benar, 1);?>%</strong>
    </td>
  </tr>
  <tr><td width="30%">Guru</td><td><b><?php echo $query[0]['nama'];?></b></td></tr>
  <tr><td width="30%">Tanggal Ujian</td><td><b><?php echo $query[0]['tanggal'];?> <?php echo $query[0]['jam_ujian'];?></b></td></tr>
  <tr><td width="30%">Waktu Ujian</td><td><b><?php echo $query[0]['waktu_ujian'];?> menit</b></td></tr>
  <tr><td width="30%">Jumlah Soal</td><td><b><?php echo $query[0]['jumlah_soal'];?> Soal</b></td></tr>
  <tr><td width="30%">Jawaban Benar</td><td><b><?php echo $benar;?></b></td></tr>
  <tr><td width="30%">Jawaban Salah</td><td><b><?php echo number_format($query[0]['jumlah_soal']-$benar);?></b></td></tr>
</table>


</body>
</html>
