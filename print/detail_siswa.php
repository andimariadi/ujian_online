<?php
require '../system/function.php';
$db = new crud();

//hak akses
if (empty($_GET['id']) OR empty($_SESSION['username'])) {
  header('location: ' . base_url('dist/'));
}

$no = $_GET['id'];

?>
<!DOCTYPE html>
<html>
<head>
  <title>Laporan Siswa</title>
  <link href='<?php echo base_url('assets/css/print_styles.css');?>' rel='stylesheet' media='' type='text/css'/>
</head>
<body onload="javascript:window.print()">

<h3>Detail Siswa</h3>
<hr style="border: solid 1px #000"><br>

<table class="" style="margin-bottom: 0px">
<?php
  $query = $db->where('t_siswa', array('nomor_induk' => $no));

  foreach ($query as $value) {
    
    echo '<tr><td><h4>Data Diri</td></tr>';
    echo '<tr><td width="30%">Nomor Induk Siswa </td><td width="1%">:</td><td><b>' . $value['nomor_induk']          . '</b></td></tr>';
    echo '<tr><td width="30%">Nama              </td><td width="1%">:</td><td><b>' . $value['nama']                 . '</b></td></tr>';
    echo '<tr><td width="30%">Jenis Kelamin     </td><td width="1%">:</td><td><b>' . $value['jk']                   . '</b></td></tr>';
    echo '<tr><td width="30%">Tempat Lahir      </td><td width="1%">:</td><td><b>' . $value['tempat_lahir']         . '</b></td></tr>';
    echo '<tr><td width="30%">Tanggal Lahir     </td><td width="1%">:</td><td><b>' . $value['tanggal_lahir']        . '</b></td></tr>';
    echo '<tr><td width="30%">Agama             </td><td width="1%">:</td><td><b>' . $value['agama']                . '</b></td></tr>';
    echo '<tr><td width="30%">Alamat            </td><td width="1%">:</td><td><b>' . $value['alamat']               . '</b></td></tr>';
    echo '<tr><td width="30%">Nomor Telpon      </td><td width="1%">:</td><td><b>' . $value['telpon']               . '</b></td></tr>';

    echo '<tr><td><br /><h4>Sekolah Asal</td></tr>';
    echo '<tr><td width="30%">Nama Sekolah      </td><td width="1%">:</td><td><b>' . $value['sekolah_asal']         . '</b></td></tr>';
    echo '<tr><td width="30%">Alamat Sekolah    </td><td width="1%">:</td><td><b>' . $value['alamat_sekolah_asal']  . '</b></td></tr>';
    echo '<tr><td width="30%">Tahun Ijazah      </td><td width="1%">:</td><td><b>' . $value['tahun_ijazah']         . '</b></td></tr>';
    echo '<tr><td width="30%">Nomor Ijazah      </td><td width="1%">:</td><td><b>' . $value['nomor_ijazah']         . '</b></td></tr>';
    echo '<tr><td width="30%">Diterima di Kelas </td><td width="1%">:</td><td><b>' . $value['diterima_dikelas']     . '</b></td></tr>';
    echo '<tr><td width="30%">Tahun Diterima    </td><td width="1%">:</td><td><b>' . $value['tahun_diterima']       . '</b></td></tr>';

    echo '<tr><td><br /><h4>Data Orang Tua</td></tr>';
    echo '<tr><td width="30%">Nama Ayah         </td><td width="1%">:</td><td><b>' . $value['nama_ayah']            . '</b></td></tr>';
    echo '<tr><td width="30%">Nama Ibu          </td><td width="1%">:</td><td><b>' . $value['nama_ibu']             . '</b></td></tr>';
    echo '<tr><td width="30%">Alamat            </td><td width="1%">:</td><td><b>' . $value['alamat_ortu']          . '</b></td></tr>';
    echo '<tr><td width="30%">Nomor Telepon     </td><td width="1%">:</td><td><b>' . $value['telp_ortu']            . '</b></td></tr>';
    echo '<tr><td width="30%">Pekerjaan         </td><td width="1%">:</td><td><b>' . $value['pekerjaan_ortu']       . '</b></td></tr>';

    echo '<tr><td><br /><h4>Data Wali</td></tr>';
    echo '<tr><td width="30%">Nama Wali         </td><td width="1%">:</td><td><b>' . $value['nama_wali']            . '</b></td></tr>';
    echo '<tr><td width="30%">Alamat            </td><td width="1%">:</td><td><b>' . $value['alamat_wali']          . '</b></td></tr>';
    echo '<tr><td width="30%">Nomor Telepon     </td><td width="1%">:</td><td><b>' . $value['telp_wali']            . '</b></td></tr>';
    echo '<tr><td width="30%">Pekerjaan         </td><td width="1%">:</td><td><b>' . $value['pekerjaan_wali']       . '</b></td></tr>';
}

?>
</table>


</body>
</html>
