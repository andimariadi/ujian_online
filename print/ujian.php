<?php
require '../system/function.php';
$db = new crud();

//hak akses
if (empty($_SESSION['username']) or empty($_GET['id'])) {
  header('location: ' . base_url('dist/'));
}

$no = $_GET['id'];

?>
<!DOCTYPE html>
<html>
<head>
  <title>Laporan Ujian</title>
  <link href='<?php echo base_url('assets/css/print_styles.css');?>' rel='stylesheet' media='' type='text/css'/>
</head>
<body onload="javascript:window.print()">

<h3>Laporan Ujian Siswa</h3>
<hr style="border: solid 1px #000"><br>

<h4>Detail Ujian</h4>
<table class="" style="margin-bottom: 0px">
<?php


$list = $db->query('SELECT * FROM `t_ujian` LEFT JOIN `t_guru` ON `t_ujian`.`nip_guru`=`t_guru`.`nip` LEFT JOIN `t_mapel` ON `t_ujian`.`kode_mapel`=`t_mapel`.`kode_mapel` WHERE `t_ujian`.`id_ujian`=\'' . $no . '\'')->fetch_all(MYSQLI_ASSOC);

foreach ($list as $element) {
  echo "<tr><td width=\"40%\">Nama Ujian      </td><td width=\"1%\">:</td><td><b>{$element['nama_ujian']}</b></td></tr>";
  echo "<tr><td width=\"40%\">Mata Pelajaran  </td><td width=\"1%\">:</td><td><b>{$element['kode_mapel']} / {$element['nama_mapel']}</b></td></tr>";
  echo "<tr><td width=\"40%\">Guru            </td><td width=\"1%\">:</td><td><b>{$element['nama']}</b></td></tr>";
  echo "<tr><td width=\"40%\">Jumlah Soal     </td><td width=\"1%\">:</td><td><b>{$element['jumlah_soal']}</b></td></tr>";
  echo "<tr><td width=\"40%\">Jam / Tanggal   </td><td width=\"1%\">:</td><td><b>{$element['jam_ujian']} / {$element['tanggal']}</b></td></tr>";
  echo "<tr><td width=\"40%\">Lama Ujian      </td><td width=\"1%\">:</td><td><b>{$element['waktu_ujian']} menit</b></td></tr>";
}


?>
</table>

<br/>
<h4>Hasil Peserta</h4>
<table class="table-bordered" style="margin-bottom: 0px">
<thead>
  <th>NIS</th>
  <th>Nama Peserta</th>
  <th>Benar</th>
  <th>Salah</th>
  <th>Score</th>
</thead>
<tbody>
<?php

$status = $db->query("SELECT * FROM `t_cat_status` LEFT JOIN `t_siswa` ON `t_cat_status`.`nomor_induk` = `t_siswa`.`nomor_induk` WHERE `status`='T' AND `t_cat_status`.`id_ujian`='{$no}'")->fetch_all(MYSQLI_ASSOC);
foreach ($status as $value) {
  //menghitung benar salahnya
  $hitung_nilai = $db->query("SELECT `t_cat`.`list_jawaban`,`t_soal`.`jawaban` FROM `t_cat` LEFT JOIN `t_soal` ON `t_cat`.`list_soal` = `t_soal`.`id_soal` WHERE `t_cat`.`id_ujian`='{$no}' AND `t_cat`.`nomor_induk_siswa`='{$value['nomor_induk']}'")->fetch_all(MYSQLI_ASSOC);
  $benar = 0;
  foreach ($hitung_nilai as $element) {
    if ($element['list_jawaban'] == $element['jawaban']) {
      $benar++;
    }
  }
  
  echo '<tr>';
  echo '<td>' . $value['nomor_induk'] . '</td>';
  echo '<td>' . $value['nama'] . '</td>';
  echo '<td>' . $benar . '</td>';
  echo '<td>' . number_format($list[0]['jumlah_soal']-$benar) . '</td>';
  echo '<td><strong>' . number_format(100/$list[0]['jumlah_soal']*$benar, 2) . '%</strong></td>';
  echo '</tr>';
          
}
?>  
</tbody>
</table>


</body>
</html>
