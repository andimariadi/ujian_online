<?php
require '../system/function.php';
$db = new crud();

//hak akses
if (empty($_SESSION['username'])) {
  header('location: ' . base_url('dist/'));
}

?>
<!DOCTYPE html>
<html>
<head>
  <title>Laporan Siswa</title>
  <link href='<?php echo base_url('assets/css/print_styles.css');?>' rel='stylesheet' media='' type='text/css'/>
</head>
<body onload="javascript:window.print()">

<h3>Laporan Siswa</h3>
<hr style="border: solid 1px #000"><br>

<table class="table-bordered" style="margin-bottom: 0px">
<thead>
  <th>NIS</th>
  <th>Nama</th>
  <th>JK</th>
  <th>Tempat, Tanggal Lahir</th>
  <th>Agama</th>
  <th>Alamat</th>
  <th>Telp.</th>
</thead>
<?php
  $query = $db->view('t_siswa');

  foreach ($query as $value) {
  echo '<tr>';
    echo '<td>' . $value['nomor_induk'] . '</td>';
    echo '<td>' . $value['nama'] . '</td>';
    echo '<td>' . $value['jk'] . '</td>';
    echo '<td>' . $value['tempat_lahir'] . '</td>';
    echo '<td>' . $value['agama'] . '</td>';
    echo '<td>' . $value['alamat'] . '</td>';
    echo '<td>' . $value['telpon'] . '</td>';
  echo '</tr>';
}

?>
</table>


</body>
</html>
