<?php
require '../system/function.php';
$db = new crud();

//hak akses
$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if (empty($_SESSION['username']) OR ($res[0]['level']=='murid')) {
  header('location: ' . base_url('dist/'));
}

?>
<!DOCTYPE html>
<html>
<head>
  <title>Printed Soal</title>
  <link href='<?php echo base_url('assets/css/print_styles.css');?>' rel='stylesheet' media='' type='text/css'/>
</head>
<body onload="javascript:window.print()">

<h3>Printed Semua Soal</h3>
<hr style="border: solid 1px #000"><br>

<table class="" style="margin-bottom: 0px">
<?php


if ($res[0]['level'] == 'guru') {

  $list = $db->query('SELECT * FROM `t_soal` LEFT JOIN `t_guru` ON `t_soal`.`nip_guru` = `t_guru`.`nip` LEFT JOIN `t_mapel`ON `t_soal`.`kode_mapel` = `t_mapel`.`kode_mapel` WHERE `t_soal`.`nip_guru`=\'' . substr($res[0]['username'], 2) . '\'')->fetch_all(MYSQLI_ASSOC);

} elseif ($res[0]['level'] == 'admin') {
  $list = $db->query('SELECT * FROM `t_soal` LEFT JOIN `t_guru` ON `t_soal`.`nip_guru` = `t_guru`.`nip` LEFT JOIN `t_mapel`ON `t_soal`.`kode_mapel` = `t_mapel`.`kode_mapel` ')->fetch_all(MYSQLI_ASSOC);
}

$no = 0;
foreach ($list as $element) {
$no++;
echo "<tr><td width=\"1%\"><p>{$no}. </p></td> <td width=\"1%\"></td><td width=\"98%\">{$element['soal']}</td></tr>";
echo "<tr><td></td> <td><p>A.</p></td><td> {$element['opsi_a']}</td></tr>";
echo "<tr><td></td> <td><p>B.</p></td><td> {$element['opsi_b']}</td></tr>";
echo "<tr><td></td> <td><p>C.</p></td><td> {$element['opsi_c']}</td></tr>";
echo "<tr><td></td> <td><p>D.</p></td><td> {$element['opsi_d']}</td></tr>";
echo "<tr><td></td> <td><p>E.</p></td><td> {$element['opsi_e']}</td></tr>";
echo "<tr><td></td> <td></td><td><b>Jawaban : " . strtoupper($element['jawaban']) . "</b></td></tr>";
echo "<tr><td></td> <td></td><td><b><br /></b></td></tr>";



}


?>
</table>


</body>
</html>
