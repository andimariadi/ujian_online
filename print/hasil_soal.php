<?php
require '../system/function.php';
$db = new crud();

//hak akses
if ((empty($_GET['nis']) or empty($_GET['id'])) OR empty($_SESSION['username'])) {
  header('location: ' . base_url('dist/'));
}

$nis = $_GET['nis'];
$no = $_GET['id'];

?>
<!DOCTYPE html>
<html>
<head>
  <title>Laporan Hasil Ujian</title>
  <link href='<?php echo base_url('assets/css/print_styles.css');?>' rel='stylesheet' media='' type='text/css'/>
</head>
<body onload="javascript:window.print()">

<h3>Laporan Hasil Ujian</h3>
<hr style="border: solid 1px #000"><br>


<h4>Detil Peserta</h4>

<table class="table-bordered" style="margin-bottom: 0px">

<?php
  $peserta = $db->query("SELECT * FROM `t_cat_status`
LEFT JOIN `t_siswa` ON `t_cat_status`.`nomor_induk`=`t_siswa`.`nomor_induk`
LEFT JOIN `t_ujian` ON `t_cat_status`.`id_ujian` = `t_ujian`.`id_ujian` WHERE `t_cat_status`.`nomor_induk`='{$nis}' AND `t_cat_status`.`id_ujian`='{$no}'")->fetch_all(MYSQLI_ASSOC);

//menghitung benar salahnya
$hitung_nilai = $db->query("SELECT `t_cat`.`list_jawaban`,`t_soal`.`jawaban` FROM `t_cat` LEFT JOIN `t_soal` ON `t_cat`.`list_soal` = `t_soal`.`id_soal` WHERE `t_cat`.`id_ujian`='{$no}' AND `t_cat`.`nomor_induk_siswa`='{$nis}'")->fetch_all(MYSQLI_ASSOC);
$benar = 0;
foreach ($hitung_nilai as $element) {
  if ($element['list_jawaban'] == $element['jawaban']) {
    $benar++;
  }
}

?>
  <tr><td width="30%">Nomor Induk Siswa</td><td><b><?php echo $peserta[0]['nomor_induk'];?></b></td><td style="text-align: center;">Score</td></tr>
  <tr><td>Nama Lengkap</td><td width="50%"><b><?php echo $peserta[0]['nama'];?></b></td><td rowspan="6" style="vertical-align: middle;text-align: center;font-weight: 800;font-size: 2em"><?php echo number_format(100/$peserta[0]['jumlah_soal']*$benar, 1);?>%</td></tr>
  <tr><td>Nama Ujian</td><td width="50%"><b><?php echo $peserta[0]['nama_ujian'];?></b></td></tr>
  <tr><td>Tanggal Ujian</td><td width="50%"><b><?php echo $peserta[0]['tanggal'] . ' / ' . $peserta[0]['jam_ujian'];?></b></td></tr>
  <tr><td>Jumlah Soal / Dijawab</td><td width="50%"><b><?php echo $peserta[0]['jumlah_soal'];?> / <span style="color: red"><?php echo count($hitung_nilai);?></span></b></td></tr>
  <tr><td>Benar</td><td width="50%"><b><?php echo $benar;?></b></td></tr>
  <tr><td>Salah</td><td  width="50%"><b><?php echo $peserta[0]['jumlah_soal']-$benar;?></b></td></tr>
</table>
<br><br>


<?php

//menghitung benar salahnya
$query = $db->query("SELECT * FROM `t_cat` LEFT JOIN `t_soal` ON `t_cat`.`list_soal` = `t_soal`.`id_soal` WHERE `nomor_induk_siswa`='{$nis}' AND `id_ujian`='{$no}'");
$query = $query->fetch_all(MYSQLI_ASSOC);



?>

<h4>Hasil Soal</h4>
<table class="table-bordered">
<thead>
  <th width="1%">No.</th>
  <th width="50%">Soal</th>
  <th>Jawaban</th>
  <th width="7%">Salah</th>
  <th width="7%">Benar</th>
</thead>
<?php

$no = 0;
foreach ($query as $value) {
  $no++;
  echo '<tr>';
    echo '<td><p>' . $no . '.</p></td>';
    echo '<td>' . $value['soal'] . '</td>';
    echo '<td>' . $value['opsi_' . $value['list_jawaban'] ] . '</td>';
    echo '<td>';
      if ($value['jawaban'] != $value['list_jawaban']) {
        echo '<p style="text-align: center;">V</p>';
      }
    echo '</td>';
    echo '<td>';
      if ($value['jawaban'] == $value['list_jawaban']) {
        echo '<p style="text-align: center;">V</p>';
      }
    echo '</td>';
  echo '</tr>';
}

?>
</table>


</body>
</html>
