<?php
require '../system/function.php';
$db = new crud();

//hak akses
$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
if ($res[0]['level'] != "murid") {
  header('location: ' . base_url('dist/index.php'));
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mulai Ujian - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
        <h1>Daftar Halaman Ujian</h1>
        
      <div class="table-inner">
        <table class="table table-hover table-striped">
          <thead>
            <tr>
              <th>Nama Tes</th>
              <th>Mata Pelajaran</th>
              <th>Guru</th>
              <th>Kelas</th>
              <th>Jumlah Soal</th>
              <th>Waktu</th>
              <th>Lama Ujian</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
          <?php

          $list = $db->query('SELECT * FROM `t_ujian` LEFT JOIN `t_guru` ON `t_ujian`.`nip_guru`=`t_guru`.`nip` LEFT JOIN `t_mapel` ON `t_ujian`.`kode_mapel`=`t_mapel`.`kode_mapel`
            LEFT JOIN `t_kelas` ON `t_ujian`.`id_kelas`=`t_kelas`.`id_kelas`
            ')->fetch_all(MYSQLI_ASSOC);

          foreach ($list as $element) {
            echo "<tr>";
            echo "
              <td>{$element['nama_ujian']}</td>
              <td>{$element['kode_mapel']} / {$element['nama_mapel']}</td>
              <td>{$element['nip']} / {$element['nama']}</td>
              <td>{$element['kelas']}</td>
              <td>{$element['jumlah_soal']}</td>
              <td>{$element['jam_ujian']} / {$element['tanggal']}</td>
              <td>{$element['waktu_ujian']} menit</td>
              ";
            echo "<td>";
            //pembuatan waktu ujian berakhir atau tidak
            $date = date_create($element['tanggal'] . ' ' . $element['jam_ujian']);
            date_add($date, date_interval_create_from_date_string($element['waktu_ujian'] . ' minutes'));
            if (date_format($date, 'Y-m-d H:i') <= date('Y-m-d H:i')) {
              echo "<a href=\"#\" class=\"btn btn-danger btn-xs\">Ujian telah berakhir</a>";
            } elseif (date('Y-m-d') != date('Y-m-d' , strtotime($element['tanggal']))) {
              echo "<a href=\"#\" class=\"btn btn-warning btn-xs\">Ujian belum dilaksanakan</a>";
            } elseif ( (date('Y-m-d H:i') <= date('Y-m-d H:i' , strtotime($element['tanggal'] . ' ' . $element['jam_ujian']))) OR (date('Y-m-d H:i') <= date_format($date, 'Y-m-d H:i'))) {
              echo "<a href=\"" . base_url('dist/detail_ujian.php?no=') . $element['id_ujian'] . "\" class=\"btn btn-primary btn-xs\">Ikut ujian</a>";
            }

            echo "</td>";
            echo "</tr>";
          }


          ?>
          </tbody>
        </table>
      </div>


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>
    <script type="text/javascript">
      function hapus($value) {
        swal({
          title: "Are you sure?",
          text: "Semua yang berhubungan dengan data ini akan terhapus otomatis!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        function(isConfirm){
          if (isConfirm) {
            window.location.assign('<?php echo base_url('delete/soal.php?no=');?>' + $value);
          }
        });
      }
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.collapse').collapse('hide');
        $('#collapseFive').collapse('show');
    });
    </script>
  </body>
</html>