<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Dashboard - Computer Assisten Test</title>
    
    <?php require '../system/function.php';?>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
<?php

$db = new crud();

if (empty($_SESSION['username'])) {
  header('location: ' . base_url('index.php'));
}

?>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
        <?php require '../include/sidebar.php'; ?>
      </div>
      <div class="col-md-9 white-body">
        <h1>Selamat Datang!</h1>
        <div class="alert alert-info" role="alert"><p>Berikut hasil data yang ada pada database CAT (Computer Assisted Test).</p></div>
        <div class="panel-body row">

          <div class="col-sm-3">
            <span class="label label-success col-xs-12 tinggi-body">
              <p>Jumlah Guru</p>
              <span class="big-title"><?php

              $guru = $db->view('t_guru');
              echo count($guru);
              ?></span>
            </span>
          </div>

          <div class="col-sm-3">
            <span class="label label-danger col-xs-12 tinggi-body">
              <p>Jumlah Siswa</p>
              <span class="big-title">
              <?php
              $siswa = $db->view('t_siswa');
              echo count($siswa);
              ?>
              </span>
            </span>
          </div>

          <div class="col-sm-3">
            <span class="label label-warning col-xs-12 tinggi-body">
              <p>Jumlah Soal</p>
              <span class="big-title">
              <?php
              $soal = $db->view('t_soal');
              echo count($soal);
              ?>
              </span>
            </span>
          </div>

          <div class="col-sm-3">
            <span class="label label-primary col-xs-12 tinggi-body">
              <p>Jumlah Ujian</p>
              <span class="big-title">
              <?php
              $ujian = $db->view('t_ujian');
              echo count($ujian);
              ?>
              </span>
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>
  </body>
</html>