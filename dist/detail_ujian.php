<?php
require '../system/function.php';
$db = new crud();
$no = mysqli_real_escape_string($db->connection, $_GET['no']);
$nis = substr($_SESSION['username'], 2);

//hak akses

$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);

if (!empty($res[0]['level']) != "murid") {
  header('location: ' . base_url('dist/index.php'));
}


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mulai Ujian - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
<?php
$query              = $db->where('t_ujian', array('id_ujian' => $no));
$result             = $query->fetch_all(MYSQLI_ASSOC);

if (isset($_POST['terus'])) {
  $token              = mysqli_real_escape_string($db->connection, $_POST['token']);

  if ($result[0]['token'] == $token) {
    $status = $db->query("SELECT `status` FROM `t_cat_status` WHERE `nomor_induk`='{$nis}' AND `id_ujian`='{$no}' AND `status`='F'");
    $status = $status->fetch_all(MYSQLI_ASSOC);

    if (mysqli_num_rows($status) > 0) {
      //menyimpan status ujian di t_cat_status
      $simpan = $db->insert('t_cat_status', array('nomor_induk' => $nis, 'id_ujian' => $no, 'status' => 'F'));
    } else {
      // update di table cat status
      $simpan = $db->query("UPDATE `t_cat_status` SET `status`='F' WHERE `nomor_induk`='{$nis}' AND `id_ujian`='{$no}'");
      
    }
    
    $_SESSION['id_ujian'] = $no;
    header('location: ' . base_url('dist/cat_tes.php'));
  } else {
    echo '<script type="text/javascript">
        swal({
        title: "Error!",
        text: "Token salah, coba lagi!",
        type: "error",
        confirmButtonText: "Oke"
      });</script>';
  }
  
}

if (isset($_POST['mulai'])) {
  $token              = mysqli_real_escape_string($db->connection, $_POST['token']);
  
  if ($result[0]['token'] == $token) {
    //generate session for ujian
    $_SESSION['id_ujian'] = $no;
    $_SESSION['kelas']    = $result[0]['id_kelas'];

    $status = $db->query("SELECT `status` FROM `t_cat_status` WHERE `nomor_induk`='{$nis}' AND `id_ujian`='{$no}' AND `status`='T'");
    $status = $status->fetch_all(MYSQLI_ASSOC);
    if ($status) {
      // update di table cat status
      $db->query("UPDATE `t_cat_status` SET `status`='F' WHERE `nomor_induk`='{$nis}' AND `id_ujian`='{$no}'");
    } else {
      //menyimpan status ujian di t_cat_status
      $simpan = $db->insert('t_cat_status', array('nomor_induk' => $nis, 'id_ujian' => $no, 'status' => 'F'));
    }
    

    if (!$simpan) {
      echo '<script type="text/javascript">
      swal({
        title: "Great!",
        text: "Token diterima, Anda akan dialihkan kehalaman soal!",
        timer: 1000,
        type: "success",
        showConfirmButton: false
      }, function() {
              window.location.assign(\'' . base_url('dist/cat_tes.php') . '\')
      });
      </script>';    
    }
      
  } else {
    echo '<script type="text/javascript">
        swal({
        title: "Error!",
        text: "Token salah, coba lagi!",
        type: "error",
        confirmButtonText: "Oke"
      });</script>';
  }
}

?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
        <h1>Detail Ujian</h1>

        <div class="col-md-8">
          <div class="panel panel-default">
            <div class="panel-body">
              <form method="post">
              <table class="table table-bordered">
              <?php
                if (!empty($no)) {
                  $query = $db->query("SELECT `nama_ujian`,`nip_guru`,`t_guru`.`nama`,`t_mapel`.`kode_mapel`,`t_mapel`.`nama_mapel`,`jumlah_soal`,`jam_ujian`,`tanggal`,`waktu_ujian`,`kelas` FROM `t_ujian` LEFT JOIN `t_guru` ON `t_ujian`.`nip_guru` = `t_guru`.`nip`LEFT JOIN `t_mapel` ON `t_ujian`.`kode_mapel` = `t_mapel`.`kode_mapel`
                    LEFT JOIN `t_kelas` ON `t_ujian`.`id_kelas`=`t_kelas`.`id_kelas`
                   WHERE `id_ujian` = '{$no}'");

                  foreach ($query as $element) {

                    //mencari selisih waktu habis
                    $date = date_create($element['tanggal'] . ' ' . $element['jam_ujian']);
                    date_add($date, date_interval_create_from_date_string($element['waktu_ujian'] . ' minutes'));

                    echo "<tr><td width=\"35%\">Nama Ujian</td><td width=\"65%\">{$element['nama_ujian']}</td></tr>";
                    echo "<tr><td>Mata Pelajaran</td><td>{$element['kode_mapel']} / {$element['nama_mapel']}</td></tr>";
                    echo "<tr><td>NIP / Guru</td><td>{$element['nip_guru']} / {$element['nama']}</td></tr>";
                    echo "<tr><td>Kelas</td><td>{$element['kelas']}</td></tr>";
                    echo "<tr><td>Jumlah Soal</td><td>{$element['jumlah_soal']}</td></tr>";
                    echo "<tr><td>Tanggal Ujian</td><td>" . substr($element['jam_ujian'], 0, 5) . " {$element['tanggal']}</td></tr>";
                    echo "<tr><td>Lama Waktu</td><td>{$element['waktu_ujian']} menit</td></tr>";
                    echo "<tr><td>Token</td><td><div class=\"col-md-5\"><input type=\"text\" name=\"token\" id=\"token\" required=\"true\" class=\"form-control\"></div></td></tr>";


                    //selisih jam
                    //$now = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
                    //$countDownDate = mktime(date('H', strtotime($element['jam_ujian'])) , date('i', strtotime($element['jam_ujian'])) , date('s', strtotime($element['jam_ujian'])) , date('d', strtotime($element['tanggal'])) , date('m', strtotime($element['tanggal'])) , date('Y', strtotime($element['tanggal'])));
                    $countDownDate  = strtotime($element['tanggal'] . $element['jam_ujian']);
                    $counthabis     = strtotime(date_format($date, 'Y-m-d H:i:s'));
                    echo "<script type=\"text/javascript\">
                var countDownDate = '$countDownDate';
                var counthabis = '$counthabis';
              </script>";


                  }
                }
              ?>
              
              </table>
              <div id="demo" style="text-align: center;"></div>              
              </form>
            </div>
          </div>
          
        </div>
        <div class="col-md-4 alert alert-info" role="alert">
          <p><strong>Token</strong> : untuk mengisi token harap tanyakan kepada guru yang memberikan ujian</p>
          <br />
          <div class="panel panel-default">
            <div class="panel-title" style="text-align: center; text-transform: uppercase;">
              Detail Peserta
            </div>
            <?php
            $otoritasi   = $db->where('t_siswa', array('nomor_induk' => $nis));
            $res         = $otoritasi->fetch_all(MYSQLI_ASSOC);
            if (!empty($res)) {
              ?>
              <div class="panel-body">
                  <table class="table table-bordered">
                    <tr><td width="10%">Nama</td><td><?php echo $res[0]['nama'];?></td></tr>
                    <tr><td width="10%">NIS</td><td><?php echo $res[0]['nomor_induk'];?></td></tr>
                  </table>
                </div>
              </div>
              <?php
            }
            ?>
        </div>

      


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>
    <script type="text/javascript">
    $(document).ready(function() {
      var x = setInterval(function() {
        $.ajax({
          url: 'sisa_waktu.php',
          data: {'t': countDownDate, 'status': 'f'},
          success: function(data) {
            <?php
            //cari sudah ujian belum
            $status = $db->query("SELECT `status` FROM `t_cat_status` WHERE `nomor_induk`='{$nis}' AND `id_ujian`='{$no}' AND `status`='T'")->fetch_all(MYSQLI_ASSOC);
          
            if ($status) {
            echo '$("#demo").html(\'<a href="' . base_url("dist/final_tes.php?id={$no}&nis={$nis}") . '" class="btn btn-danger btn-lg">Anda Telah Mengikuti Ujian</a>\');';
            } else {

            ?>
            if (data <= 0) {
              <?php
                //cari ujian yang dimiliki
                $query       = $db->query("SELECT * FROM `t_cat_status` WHERE `id_ujian`='$no' AND `nomor_induk`='" . $nis . "'");
                if (mysqli_num_rows($query) > 0) {
                  echo '$("#demo").html(\'<button name="terus" type="submit" class="btn btn-warning btn-lg">Teruskan Ujian</button>\')';
                } else {
                  echo '$("#demo").html(\'<button type="submit" name="mulai" class="btn btn-success btn-lg">Mulai</button>\')';
                }
              ?>

              $.ajax({
                url: 'sisa_waktu.php',
                data: {'t': counthabis, 'status': 't'},
                success: function(data) {
                  if (data <= 0) {
                    clearInterval(x);
                    $("#demo").html('<a href="#" class="btn btn-danger btn-lg">Session Habis</a>');
                  }
                }
              })
            } else {
              $("#demo").html('<a href="#" class="btn btn-danger btn-lg">' + data +'</a>');
            }
           <?php
          //end pencarian statuss
            }

          ?>

          }
        })

      }, 1000);

      $('.collapse').collapse('hide');
      $('#collapseFive').collapse('show');
    });
    </script>
  </body>
</html>