<?php
$t 				= $_GET['t'];
$sekarang 		= time('now');
if (isset($_GET['status']) == 'f') {
	$selisih 		= $t-$sekarang;
} elseif (isset($_GET['status']) == 't') {
	$selisih 		= $sekarang-$t;
}

$hari 			= number_format($selisih/86400);
$jam 			= number_format($selisih % 86400 / 3600)-1;
$menit 			= number_format($selisih % 3600 / 60);
$detik 			= number_format($selisih % 60 / 1);

if ($detik <= 0 && $menit <= 0 && $jam <= 0) {
	$sisa = 0;
} else {
	if ($jam < 10) {
		if ($jam < 0) {
			$jam = 0;
		}
		$jam = 0 . $jam;
	}
	if ($menit < 10) {
		$menit = 0 . $menit;
	}
	if ($detik < 10) {
		$detik = 0 . $detik;
	}
	$sisa = $jam.':'.$menit.':'.$detik;
}




echo "{$sisa}";
?>