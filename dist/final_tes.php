<?php
require '../system/function.php';
$db = new crud();


//cari status ujian
//mengetahui sudah ujian atau belum
if ((empty($_GET['nis']) or empty($_GET['id'])) OR empty($_SESSION['username'])) {
  header('location: ' . base_url('dist/ujian.php'));
}

$nis = $_GET['nis'];
$no = $_GET['id'];

if (isset($_SESSION['id_ujian']) AND isset($_SESSION['page'])) {
  $status = $db->query("SELECT `status` FROM `t_cat_status` WHERE `nomor_induk`='{$nis}' AND `id_ujian`='{$no}'");
  $status = $status->fetch_all(MYSQLI_ASSOC);
  if (($status[0]['status'] == 'F') OR (empty($status))) {
    // update di table cat status
    $db->query("UPDATE `t_cat_status` SET `status`='T' WHERE `nomor_induk`='{$nis}' AND `id_ujian`='{$no}'");
  }

  //hapus session ujian
  unset($_SESSION['id_ujian']);
  unset($_SESSION['page']);
  unset($_SESSION['kelas']);
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Hasil Ujian - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <?php require '../include/sidebar.php'; ?>
        
      </div>
      <div class="col-md-9 white-body">
      <p>
        <h1>Daftar Halaman Ujian</h1>

        <a href="#" class="btn btn-default" onclick="print('<?php echo base_url("print/hasil_ujian.php?id=$no&nis=$nis");?>')"><span class="glyphicon glyphicon-print" style="margin-right: 15px;"></span> Print</a>

        <a href="#" class="btn btn-default" onclick="print('<?php echo base_url("print/hasil_soal.php?id=$no&nis=$nis");?>')"><span class="glyphicon glyphicon-print" style="margin-right: 15px;"></span> Print Jawaban</a>
      </p>
        
      
      <div class="table-inner">
      	<div class="alert alert-info" role="alert">
          <h3 class="text-center">Detail Peserta</h3>
          <?php
            $peserta = $db->where('t_siswa', array('nomor_induk' => $nis))->fetch_all(MYSQLI_ASSOC);
          ?>
          <table class="table table-bordered">
            <tr>
              <td width="30%"><strong>Nama Peserta</strong></td>
              <td><?php echo $peserta[0]['nama'];?></td>
            </tr>
            <tr>
              <td width="30%"><strong>Nomor Induk</strong></td>
              <td><?php echo $peserta[0]['nomor_induk'];?></td>
            </tr>

          </table>
      		<h3 class="text-center">Hasil Akhir Ujian</h3>
      		<?php

          //menghitung benar salahnya
      		$hitung_nilai = $db->query("SELECT `t_cat`.`list_jawaban`,`t_soal`.`jawaban` FROM `t_cat` LEFT JOIN `t_soal` ON `t_cat`.`list_soal` = `t_soal`.`id_soal` WHERE `t_cat`.`id_ujian`='{$no}' AND `t_cat`.`nomor_induk_siswa`='{$nis}'")->fetch_all(MYSQLI_ASSOC);
      		$benar = 0;
      		foreach ($hitung_nilai as $element) {
      			if ($element['list_jawaban'] == $element['jawaban']) {
      				$benar++;
      			}
      		}
          
      		//echo $nilai;
          //query data ujian
          $query = $db->query("SELECT `nama_ujian`,`nip_guru`,`t_guru`.`nama`,`t_mapel`.`kode_mapel`,`t_mapel`.`nama_mapel`,`jumlah_soal`,`jam_ujian`,`tanggal`,`waktu_ujian` FROM `t_ujian` LEFT JOIN `t_guru` ON `t_ujian`.`nip_guru` = `t_guru`.`nip`LEFT JOIN `t_mapel` ON `t_ujian`.`kode_mapel` = `t_mapel`.`kode_mapel` WHERE `id_ujian` = '{$no}'");
          $query = $query->fetch_all(MYSQLI_ASSOC);
      		?>
      		<table class="table table-bordered">
      			<tr>
      				<td width="30%"><strong>Nama Ujian</strong></td>
      				<td width="30%"><?php echo $query[0]['nama_ujian'];?></td>
              <td class="text-center"><strong>Score</strong></td>
      			</tr>
            <tr>
              <td width="30%"><strong>Mata Pelajaran </strong></td>
              <td><?php echo $query[0]['kode_mapel'];?> / <?php echo $query[0]['nama_mapel'];?></td>
              <td rowspan="7" class="text-center" style="vertical-align: middle; font-size: 5em">
                <strong><?php echo number_format(100/$query[0]['jumlah_soal']*$benar, 1);?>%</strong>
              </td>
            </tr>
            <tr>
              <td width="30%"><strong>Guru</strong></td>
              <td><?php echo $query[0]['nama'];?></td>
            </tr>
            <tr>
              <td width="30%"><strong>Tanggal Ujian</strong></td>
              <td><?php echo $query[0]['tanggal'];?> <?php echo $query[0]['jam_ujian'];?></td>
            </tr>

            <tr>
              <td width="30%"><strong>Waktu Ujian</strong></td>
              <td><?php echo $query[0]['waktu_ujian'];?> menit</td>
            </tr>

            <tr>
              <td width="30%"><strong>Jumlah Soal</strong></td>
              <td><?php echo $query[0]['jumlah_soal'];?></td>
            </tr>

            <tr>
              <td width="30%"><strong>Jawaban Benar</strong></td>
              <td><?php echo $benar;?></td>
            </tr>

            <tr>
              <td width="30%"><strong>Jawaban Salah</strong></td>
              <td><?php echo number_format($query[0]['jumlah_soal']-$benar);?></td>
            </tr>

      		</table>
      	</div>
      </div>


      </div>
    </div>
  </div>

    <?php require '../include/footer.php';?>
    <script type="text/javascript">
      function hapus($value) {
        swal({
          title: "Are you sure?",
          text: "Semua yang berhubungan dengan data ini akan terhapus otomatis!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        function(isConfirm){
          if (isConfirm) {
            window.location.assign('<?php echo base_url('delete/soal.php?no=');?>' + $value);
          }
        });
      }
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.collapse').collapse('hide');
        $('#collapseFive').collapse('show');
    });
    </script>
  </body>
</html>