<?php
require '../system/function.php';
$db = new crud();

//hak akses
if (isset($_SESSION['id_ujian'])) {
  $no             = $_SESSION['id_ujian'];
} else {
  $no             = NULL;
}
$nis              = substr($_SESSION['username'], 2);
$_SESSION['page'] = 0;

//mengetahui sudah ujian atau belum
$status = $db->query("SELECT `status` FROM `t_cat_status` WHERE `nomor_induk`='{$nis}' AND `id_ujian`='{$no}' AND `status`='F'");
$status = $status->fetch_all(MYSQLI_ASSOC);

$otoritasi   = $db->where('t_user', array('username' => $_SESSION['username']));
$res         = $otoritasi->fetch_all(MYSQLI_ASSOC);

if (($res[0]['level'] != 'murid') or empty($no) or (empty($status)) or (empty($status))) {
  header('location: ' . base_url('dist/ujian.php'));
}

//query ujian wajib
$query = $db->query("SELECT `nama_ujian`,`t_mapel`.`kode_mapel`,`t_mapel`.`nama_mapel`,`jumlah_soal`,`tanggal`,`waktu_ujian`,`jam_ujian` FROM `t_ujian` LEFT JOIN `t_mapel` ON `t_ujian`.`kode_mapel` = `t_mapel`.`kode_mapel` WHERE `id_ujian`='{$no}'");
$query = $query->fetch_all(MYSQLI_ASSOC);
$date = date_create($query[0]['tanggal'] . ' ' . $query[0]['jam_ujian']);
date_add($date, date_interval_create_from_date_string($query[0]['waktu_ujian'] . ' minutes'));

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ujian <?php echo $query[0]['nama_mapel'];?> - Computer Assisten Test</title>
    <?php require '../include/head.php';?>
  </head>
  <body class="admin-body">
  <?php

  //confirm ujian berakhir
  if (date_format($date, 'Y-m-d H:i') <= date('Y-m-d H:i') ) {
    echo '<script type="text/javascript">
      swal({
        title: "Error!",
        text: "Ujian telah berakhir",
        type: "error",
        confirmButtonText: "Oke"
      },
      function() {
        window.location.assign(\'' . base_url("dist/final_tes.php?id={$no}&nis={$nis}") . '\');
      });
      </script>';
  } elseif((date('Y-m-d H:i')) < date('Y-m-d H:i', strtotime($query[0]['tanggal'] . ' ' . $query[0]['jam_ujian']))) {
    echo '<script type="text/javascript">
      swal({
        title: "Error!",
        text: "Ujian belum dimulai",
        type: "error",
        confirmButtonText: "Oke"
      },
      function() {
        window.location.assign(\'' . base_url("dist/final_tes.php?id={$no}&nis={$nis}") . '\');
      });
      </script>';
  } else {
  ?>
  <div class="container-fluid">
    <div class="row">

      <div class="col-md-9 white-body">
        <div id="listi_soal"></div>
      </div>

      
      <div class="col-md-3">
        <?php

          echo "<script type=\"text/javascript\">
                  var counthabis = '" . strtotime(date_format($date, 'Y-m-d H:i')) . "';
                </script>";
        ?>

        <div class="panel panel-default">
          <div class="panel-title" style="text-align: center;">
            <h3>Waktu tersisa</h3>
            <div id="waktu_down"></div>
          </div>
          <hr />
          <table class="table table-bordered">
            <?php

              $list = $db->where('t_siswa', array('nomor_induk' => $nis));
              $list = $list->fetch_all(MYSQLI_ASSOC);

              echo "<tr><td style=\"font-weight: 800; width: 35%;\">    Nama Ujian</td><td width=\"65%\">{$query[0]['nama_ujian']}</td></tr>";
              echo "<tr><td style=\"font-weight: 800;\">                  Mata Pelajaran</td><td>{$query[0]['kode_mapel']} / {$query[0]['nama_mapel']}</td></tr>";
              echo "<tr><td style=\"font-weight: 800;\">                  NIS / Nama</td><td>{$list[0]['nomor_induk']} / {$list[0]['nama']}</td></tr>";
              echo "<tr><td style=\"font-weight: 800;\">                  Jumlah Soal</td><td>{$query[0]['jumlah_soal']}</td></tr>";
              echo "<tr><td style=\"font-weight: 800;\">                  Tanggal Ujian</td><td>" . substr($query[0]['jam_ujian'], 0, 5) . " {$query[0]['tanggal']}</td></tr>";
              echo "<tr><td style=\"font-weight: 800;\">                  Lama Waktu</td><td>{$query[0]['waktu_ujian']} menit</td></tr>";
            ?>
          </table>
        </div>

        <div class="text-center"><a href="#" id="kirim" class="btn btn-danger btn-lg">Akhiri Ujian</a></div>
        
      </div>
    </div>
  </div>

  <script type="text/javascript">
    $(document).ready(function() {
      //interval selisih waktu
      var x = setInterval(function() {
        $.ajax({
          url: '<?php echo base_url("dist/sisa_waktu.php");?>',
          data: {'t': counthabis, 'status': 'f'},
          success: function(data) {
            if (data <= 0) {
              $("#waktu_down").html('<a href="#" class="btn btn-danger btn-lg">Waktu Habis</a>');
              swal({
                title: "Error!",
                text: "Ujian telah berakhir",
                type: "error",
                confirmButtonText: "Oke"
              },
              function() {
                window.location.assign('<?php echo base_url("dist/final_tes.php?id={$no}&nis={$nis}");?>');
              });
            } else {
              $("#waktu_down").html('<a href="#" class="btn btn-success btn-lg">' + data +'</a>');
            }           
          }
        })          
      }, 1000);

      //loader
      $("#listi_soal").load('<?php echo base_url("entry/cat_soal.php");?>');

      //prosedur next
      $(document).on("click", "#jawab", function() {
        var data = $("#soal").serialize();
        $.ajax({
          type: 'POST',
          data: data,
          url: '<?php echo base_url("entry/cat.php");?>'
        })
      });

      $(document).on("click", "#next", function() {
        $("#listi_soal").load('<?php echo base_url("entry/cat_soal.php");?>');
      });

      $(document).on("click", "#back", function() {
        $("#listi_soal").load('<?php echo base_url("entry/cat_soal.php?s=back");?>');
      });

      $(document).on("change", "#jwb", function(event) {
        var jwb = $(this).val();
        var cat = $(this).attr('id-data');
        //var data = $("form[id=all]").serialize();
        $.ajax({
          type: 'POST',
          data: {'id_cat': cat, 'jwb': jwb},
          url: '<?php echo base_url("entry/cat.php?s=update");?>'
        })
      });

      $(document).on("click", "#kirim", function(event) {
        swal({
          title: "Confirm?",
          text: "Kirim jawaban sekarang ?.",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Kirim",
          closeOnConfirm: false
        },
        function(isConfirm){
          if (isConfirm) {
            window.location.assign('<?php echo base_url("dist/final_tes.php?id={$no}&nis={$nis}");?>');
          }
        });
      })
    });
  </script>
  <?php

//end confirm ujian
}
?>
  </body>
</html>
