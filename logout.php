<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Masuk Aplikasi - Computer Assisten Test</title>
    
    <?php require 'system/function.php';?>
    <?php require 'include/head.php';?>
  </head>
  <body class="login-body">
  	<?php
  		session_destroy();
  		header('location: ' . base_url('index.php'));
  	?>
  </body>
</html>